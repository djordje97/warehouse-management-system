package ftn.poslovnainformatika.warehouse.controller;

import ftn.poslovnainformatika.warehouse.dto.CompanyDTO;
import ftn.poslovnainformatika.warehouse.dto.support.CompanyDTOToCompany;
import ftn.poslovnainformatika.warehouse.dto.support.CompanyToCompanyDTOConverter;
import ftn.poslovnainformatika.warehouse.entity.Company;
import ftn.poslovnainformatika.warehouse.service.CompanyServiceInterface;
import ftn.poslovnainformatika.warehouse.service.EmployeeServiceInterface;
import ftn.poslovnainformatika.warehouse.service.PlaceServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "api/companies")
@CrossOrigin("*")
public class CompanyController {

    @Autowired
    CompanyServiceInterface companyServiceInterface;
    @Autowired
    PlaceServiceInterface placeService;

    @Autowired
    EmployeeServiceInterface employService;

    @Autowired
    CompanyToCompanyDTOConverter toCompanyDTO;

    @Autowired
    CompanyDTOToCompany toCompany;

    /*@GetMapping
    public ResponseEntity<List<CompanyDTO>> getAll(){
        List<Company> entity=companyServiceInterface.findAll();
        List<CompanyDTO> dtos =new ArrayList<>();
        for (Company company:entity) {
           dtos.add(new CompanyDTO(company)) ;
        }

        return  new ResponseEntity<>(dtos, HttpStatus.OK);
    }*/

    @GetMapping
    public ResponseEntity<List<CompanyDTO>> getAllByPage(@RequestParam(defaultValue = "0") int pageNum){
        Page<Company> companies = companyServiceInterface.findAll(pageNum);

        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages",Integer.toString(companies.getTotalPages()));
        headers.add("access-control-expose-headers","totalPages");
        return new ResponseEntity<>(toCompanyDTO.convert(companies.getContent()),headers,HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<CompanyDTO> getById(@PathVariable("id") Integer id){
        if(companyServiceInterface.findOne(id) == null)
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return  new ResponseEntity<>(new CompanyDTO(companyServiceInterface.findOne(id)),HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> create(@Validated @RequestBody CompanyDTO dto, BindingResult result){
        if(result.hasErrors()){
            return new ResponseEntity<>(result.toString(), HttpStatus.BAD_REQUEST);
        }
        Company entity = companyServiceInterface.save(toCompany.convert(dto));

        return new ResponseEntity<>(toCompanyDTO.convert(entity),HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public  ResponseEntity<CompanyDTO> update(@RequestBody CompanyDTO dto,@PathVariable("id") Integer id){
        if(companyServiceInterface.findOne(id) == null)
            return  new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        Company entity= companyServiceInterface.findOne(id);
        entity=companyServiceInterface.save(toCompany.convert(dto));
        return new ResponseEntity<>(toCompanyDTO.convert(entity),HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Integer id){
        if(companyServiceInterface.findOne(id) == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        companyServiceInterface.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
