package ftn.poslovnainformatika.warehouse.controller;

import ftn.poslovnainformatika.warehouse.dto.EmployeeDTO;
import ftn.poslovnainformatika.warehouse.dto.support.EmployeeDTOToEmployee;
import ftn.poslovnainformatika.warehouse.dto.support.EmployeeToEmployeeDTOConverter;
import ftn.poslovnainformatika.warehouse.entity.Employee;
import ftn.poslovnainformatika.warehouse.service.CompanyServiceInterface;
import ftn.poslovnainformatika.warehouse.service.EmployeeServiceInterface;
import ftn.poslovnainformatika.warehouse.service.PlaceServiceInterface;
import ftn.poslovnainformatika.warehouse.service.WarehouseServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "api/employees")
@CrossOrigin("*")
public class EmployeeController {

    @Autowired
    EmployeeServiceInterface employeeServiceInterface;

    @Autowired
    EmployeeToEmployeeDTOConverter toEmployeeDTO;

    @Autowired
    EmployeeDTOToEmployee toEmployee;

    /*@GetMapping
    public ResponseEntity<List<EmployeeDTO>> getAll(){
        List<Employee> entity=employeeServiceInterface.findAll();
        List<EmployeeDTO> dtos=new ArrayList<>();
        for(Employee employee:entity){
            dtos.add(new EmployeeDTO(employee));
        }
        return  new ResponseEntity<>(dtos, HttpStatus.OK);
    }*/

    @GetMapping()
    public ResponseEntity<List<EmployeeDTO>> getAllByPage(@RequestParam(defaultValue = "0") int pageNum){
        Page<Employee> employees = employeeServiceInterface.findAll(pageNum);

        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages",Integer.toString(employees.getTotalPages()));
        return new ResponseEntity<>(toEmployeeDTO.convert(employees.getContent()),headers,HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public  ResponseEntity<EmployeeDTO> getById(@PathVariable("id") Integer id){

        if(employeeServiceInterface.findOne(id) == null)
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return  new ResponseEntity<>(new EmployeeDTO(employeeServiceInterface.findOne(id)) ,HttpStatus.OK);
    }

    @PostMapping
    public  ResponseEntity<?> create(@Validated @RequestBody EmployeeDTO dto, BindingResult result){
        if(result.hasErrors()){
            return new ResponseEntity<>(result.toString(), HttpStatus.BAD_REQUEST);
        }
        Employee entity=employeeServiceInterface.save(toEmployee.convert(dto));

        return  new ResponseEntity<>(toEmployeeDTO.convert(entity),HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<EmployeeDTO> update(@RequestBody EmployeeDTO dto,@PathVariable("id") Integer id){
        if(employeeServiceInterface.findOne(id) == null)
            return  new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        Employee entity=employeeServiceInterface.findOne(id);
        entity=employeeServiceInterface.save(toEmployee.convert(dto));

        return  new ResponseEntity<>(toEmployeeDTO.convert(entity),HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Integer id){
        if(employeeServiceInterface.findOne(id) == null)
            return  new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        employeeServiceInterface.delete(id);
        return  new ResponseEntity<>(HttpStatus.OK);
    }
}
