package ftn.poslovnainformatika.warehouse.controller;

import ftn.poslovnainformatika.warehouse.dto.PlaceDTO;
import ftn.poslovnainformatika.warehouse.dto.support.PlaceDTOToPlace;
import ftn.poslovnainformatika.warehouse.dto.support.PlaceToPlaceDTOConverter;
import ftn.poslovnainformatika.warehouse.entity.Place;
import ftn.poslovnainformatika.warehouse.service.PlaceServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "api/places")
@CrossOrigin("*")
public class PlaceController {

    @Autowired
    PlaceServiceInterface placeService;

    @Autowired
    PlaceToPlaceDTOConverter toPlaceDTO;

    @Autowired
    PlaceDTOToPlace toPlace;

    /*@GetMapping
    public ResponseEntity<List<PlaceDTO>> getAll(){
        List<Place> entity=placeService.findAll();
        List<PlaceDTO> dtos=new ArrayList<>();
        for(Place Place:entity){
            dtos.add(new PlaceDTO(Place));
        }
        return  new ResponseEntity<>(dtos, HttpStatus.OK);
    }*/

    @GetMapping()
    public ResponseEntity<List<PlaceDTO>> getAllByPage(@RequestParam(defaultValue = "0") int pageNum){
        Page<Place> places = placeService.findAll(pageNum);

        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages",Integer.toString(places.getTotalPages()));
        return new ResponseEntity<>(toPlaceDTO.convert(places.getContent()),headers,HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public  ResponseEntity<PlaceDTO> getById(@PathVariable("id") Integer id){

        if(placeService.findOne(id) == null)
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return  new ResponseEntity<>(new PlaceDTO(placeService.findOne(id)) ,HttpStatus.OK);
    }

    @PostMapping
    public  ResponseEntity<?> create(@Validated @RequestBody PlaceDTO dto, BindingResult result){
        if(result.hasErrors()){
            return new ResponseEntity<>(result.toString(), HttpStatus.BAD_REQUEST);
        }
        Place entity=placeService.save(toPlace.convert(dto));

        return  new ResponseEntity<>(toPlaceDTO.convert(entity),HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<PlaceDTO> update(@RequestBody PlaceDTO dto,@PathVariable("id") Integer id){
        if(placeService.findOne(id) == null)
            return  new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        Place entity=placeService.findOne(id);
        entity=placeService.save(toPlace.convert(dto));

        return  new ResponseEntity<>(toPlaceDTO.convert(entity),HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Integer id){
        if(placeService.findOne(id) == null)
            return  new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        placeService.delete(id);
        return  new ResponseEntity<>(HttpStatus.OK);
    }
}
