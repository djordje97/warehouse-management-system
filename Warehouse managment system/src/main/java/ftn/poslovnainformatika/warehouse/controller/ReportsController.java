package ftn.poslovnainformatika.warehouse.controller;


import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.core.io.InputStreamResource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

import javax.sql.DataSource;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "api/reports")
public class ReportsController {

    @Autowired
    DataSource dataSource;

    @GetMapping(value = "/lagerLista")
    public ResponseEntity getReport(@RequestParam String name) throws JRException, IOException, SQLException {
        JasperPrint jp;
        Map<String,Object> parametars = new HashMap<>();

        parametars.put("warehouseName", name);
        System.err.println(parametars);
        jp = JasperFillManager.fillReport(
                getClass().getResource("/jasper/LagerLista.jasper").openStream(),parametars, dataSource.getConnection());
        ByteArrayInputStream bis = new ByteArrayInputStream(JasperExportManager.exportReportToPdf(jp));//ExportReportToPdf vraca byte[]


        System.err.println(bis);
        System.err.println(jp);
        System.err.println(getClass().getResource("/jasper/LagerLista.jasper"));

        System.err.println(dataSource.getConnection());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=lagerLista.pdf");
        headers.add("access-control-expose-headers","Content-Disposition");
        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }

    @GetMapping(value = "/analytic")
    public ResponseEntity getReportAnalytics(@RequestParam Integer productId,
                                             @RequestParam @DateTimeFormat(pattern="ddMMyyyy") Date startDate,
                                             @RequestParam @DateTimeFormat(pattern="ddMMyyyy") Date endDate) throws JRException, IOException, SQLException {
        JasperPrint jp;
        Map<String,Object> parametars = new HashMap<>();
        //java.sql.Date sd=new java.sql.Date(startDate.getTime());
        //java.sql.Date ed=new java.sql.Date(endDate.getTime());
        parametars.put("productId", productId);
        parametars.put("startDate", startDate);
        parametars.put("endDate", endDate);
        System.err.println(parametars);
        jp = JasperFillManager.fillReport(
                getClass().getResource("/jasper/Analitika.jasper").openStream(),parametars, dataSource.getConnection());
        ByteArrayInputStream bis = new ByteArrayInputStream(JasperExportManager.exportReportToPdf(jp));//ExportReportToPdf vraca byte[]



        System.err.println(bis);
        System.err.println(jp);
        System.err.println(getClass().getResource("/jasper/Analitika.jasper"));

        System.err.println(dataSource.getConnection());

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=analitika.pdf");
        headers.add("access-control-expose-headers","Content-Disposition");
        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }
}
