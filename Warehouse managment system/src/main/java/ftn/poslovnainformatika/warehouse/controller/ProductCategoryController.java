package ftn.poslovnainformatika.warehouse.controller;

import ftn.poslovnainformatika.warehouse.dto.ProductCategoryDTO;
import ftn.poslovnainformatika.warehouse.dto.support.ProductCategoryDTOToProductCategory;
import ftn.poslovnainformatika.warehouse.dto.support.ProductCategoryToProductCategoryDTOConverter;
import ftn.poslovnainformatika.warehouse.entity.ProductCategory;
import ftn.poslovnainformatika.warehouse.service.ProductCategoryServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "api/productCategories")
@CrossOrigin("*")
public class ProductCategoryController {

    @Autowired
    ProductCategoryServiceInterface productCategoryService;

    @Autowired
    ProductCategoryToProductCategoryDTOConverter toProductCategoryDTO;

    @Autowired
    ProductCategoryDTOToProductCategory toProductCategory;

    /*@GetMapping
    public ResponseEntity<List<ProductCategoryDTO>> getAll(){
        List<ProductCategory> entity=ProductCategoryService.findAll();
        List<ProductCategoryDTO> dtos=new ArrayList<>();
        for(ProductCategory ProductCategory:entity){
            dtos.add(new ProductCategoryDTO(ProductCategory));
        }
        return  new ResponseEntity<>(dtos, HttpStatus.OK);
    }*/

    @GetMapping()
    public ResponseEntity<List<ProductCategoryDTO>> getAllByPage(@RequestParam(defaultValue = "0") int pageNum){
        Page<ProductCategory> categories = productCategoryService.findAll(pageNum);

        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages",Integer.toString(categories.getTotalPages()));
        return new ResponseEntity<>(toProductCategoryDTO.convert(categories.getContent()),headers,HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public  ResponseEntity<ProductCategoryDTO> getById(@PathVariable("id") Integer id){

        if(productCategoryService.findOne(id) == null)
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return  new ResponseEntity<>(new ProductCategoryDTO(productCategoryService.findOne(id)) ,HttpStatus.OK);
    }

    @PostMapping
    public  ResponseEntity<?> create(@Validated @RequestBody ProductCategoryDTO dto, BindingResult result){
        if(result.hasErrors()){
            return new ResponseEntity<>(result.toString(), HttpStatus.BAD_REQUEST);
        }
        ProductCategory entity = productCategoryService.save(toProductCategory.convert(dto));

        return  new ResponseEntity<>(toProductCategoryDTO.convert(entity),HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<ProductCategoryDTO> update(@RequestBody ProductCategoryDTO dto,@PathVariable("id") Integer id){
        if(productCategoryService.findOne(id) == null)
            return  new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        ProductCategory entity=productCategoryService.findOne(id);
        entity=productCategoryService.save(toProductCategory.convert(dto));

        return  new ResponseEntity<>(toProductCategoryDTO.convert(entity),HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Integer id){
        if(productCategoryService.findOne(id) == null)
            return  new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        productCategoryService.delete(id);
        return  new ResponseEntity<>(HttpStatus.OK);
    }
}
