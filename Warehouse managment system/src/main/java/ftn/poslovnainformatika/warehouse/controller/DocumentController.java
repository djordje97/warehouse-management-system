package ftn.poslovnainformatika.warehouse.controller;

import ftn.poslovnainformatika.warehouse.dto.DocumentDTO;
import ftn.poslovnainformatika.warehouse.dto.support.DocumentDTOToDocument;
import ftn.poslovnainformatika.warehouse.dto.support.DocumentToDocumentDTOConverter;
import ftn.poslovnainformatika.warehouse.entity.*;
import ftn.poslovnainformatika.warehouse.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping(value = "api/documents")
@CrossOrigin("*")
public class DocumentController {

    @Autowired
    DocumentServiceInterface documentServiceInterface;

    @Autowired
    BusinessPartnerServiceInterface businessPartnerService;

    @Autowired
    WarehouseServiceInterface warehouseService;

    @Autowired
    DocumentToDocumentDTOConverter toDocumentDTO;

    @Autowired
    DocumentDTOToDocument toDocument;

    @Autowired
    DocumentItemServiceInterface documentItemService;

    @Autowired
    ProductServiceInterface productService;

    @Autowired
    ProductCardServiceIntefrace productCardService;

    @Autowired
    WarehouseCardAnalyticServiceInterface warehouseCardAnalyticService;

   /* @GetMapping
    public ResponseEntity<List<DocumentDTO>> getAll(){
        List<Document> entity=documentServiceInterface.findAll();
        List<DocumentDTO>dtos=new ArrayList<>();
        for(Document document:entity){
            dtos.add(new DocumentDTO(document));
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }*/

    @GetMapping()
    public ResponseEntity<List<DocumentDTO>> getAllByPage(@RequestParam(defaultValue = "0") int pageNum){
        Page<Document> documents = documentServiceInterface.findAll(pageNum);

        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages",Integer.toString(documents.getTotalPages()));
        headers.add("access-control-expose-headers","totalPages");
        return new ResponseEntity<>(toDocumentDTO.convert(documents.getContent()),headers,HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<DocumentDTO> getById(@PathVariable("id") Integer id){
        if(documentServiceInterface.findOne(id) == null)
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(new DocumentDTO(documentServiceInterface.findOne(id)),HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> create(@Validated @RequestBody DocumentDTO dto, BindingResult result){
        if(result.hasErrors()){
            return new ResponseEntity<>(result.toString(), HttpStatus.BAD_REQUEST);
        }
        Document entity=null;
        if(dto.getStatus().equals("Storno")){
             entity=documentServiceInterface.findOne(dto.getId());
             Document newDocument= new Document(entity);
             newDocument.setDocumentItems(new HashSet<>());
             newDocument.setStatus("Stornirano");
            for (DocumentItem item:documentItemService.getByDocument(entity.getId())) {
                Product product=productService.findOne(item.getProduct().getId());
                ProductCard productCard=product.getProductCard();
                WarehouseCardAnalytic cardAnalytic = new WarehouseCardAnalytic();
                newDocument = documentServiceInterface.save(newDocument);
                if(entity.getType().equals("Primka")){
                    productCard.setTrafficEntryQuantity(productCard.getTrafficEntryQuantity()-item.getQuantity());
                    productCard.setTrafficEntryValue(productCard.getTrafficEntryValue()-item.getValue());
                    cardAnalytic.setDirection("I");
                    cardAnalytic.setTrafficType("PR");
                    cardAnalytic.setQuantity(-item.getQuantity());
                    cardAnalytic.setPrice(item.getPrice());
                    cardAnalytic.setTotalValue(-item.getValue());cardAnalytic.setOrdinalNumber(new Random().nextInt(1000 - 1) + 1);
                    cardAnalytic.setProductCard(productCard);
                }else{
                    productCard.setTrafficExitQuantity(productCard.getTrafficExitQuantity()-item.getQuantity());
                    productCard.setTrafficExitValue(productCard.getTrafficExitValue()-item.getValue());
                    cardAnalytic.setDirection("U");
                    cardAnalytic.setTrafficType("OT");
                    cardAnalytic.setQuantity(-item.getQuantity());
                    cardAnalytic.setPrice(item.getPrice());
                    cardAnalytic.setTotalValue(-item.getValue());
                    cardAnalytic.setOrdinalNumber(new Random().nextInt(1000 - 1) + 1);
                    cardAnalytic.setProductCard(productCard);
                }
                productCard.setTotalQuantity();
                productCard.setTotalValue();
                productService.save(product);
                productCardService.save(productCard);

                DocumentItem newItem=new DocumentItem(item);
                newItem.setDocument(newDocument);
                newItem.setWarehouseCardAnalytics(new HashSet<>());
                newItem=documentItemService.save(newItem);
                cardAnalytic.setDocumentItem(newItem);
                warehouseCardAnalyticService.save(cardAnalytic);
            }
        }else{
            entity = documentServiceInterface.save(toDocument.convert(dto));
        }


        return  new ResponseEntity<>(toDocumentDTO.convert(entity),HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public  ResponseEntity<DocumentDTO> update(@RequestBody DocumentDTO dto,@PathVariable("id") Integer id){
        if(documentServiceInterface.findOne(id) == null)
            return  new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        Document entity=toDocument.convert(dto);

        entity.setBookingDate(new Date());
        List<DocumentItem> documentItems=documentItemService.getByDocument(entity.getId());
           if(documentItems.size()>0){
               for (DocumentItem item:documentItems) {
                   Product product=productService.findOne(item.getProduct().getId());
                   ProductCard productCard=product.getProductCard();
                         WarehouseCardAnalytic wca=null;
                        WarehouseCardAnalytic cardAnalytic = new WarehouseCardAnalytic();
                        if (entity.getType().equals("Primka")) {
                            productCard.setTrafficEntryQuantity(productCard.getTrafficEntryQuantity()+item.getQuantity());
                            productCard.setTrafficEntryValue(productCard.getTrafficEntryValue()+item.getValue());
                            productCard.setTotalValue();
                            productCard.setTotalQuantity();
                            double oldPrice = productCard.getPrice();
                            double newPrice = productCard.getCalculatePrice();
                            if(newPrice != oldPrice){
                                productCard.setPrice(newPrice);
                               wca = new WarehouseCardAnalytic();
                                wca.setDirection("U");
                                wca.setTrafficType("NI");
                                wca.setDocumentItem(item);
                                wca.setQuantity(0.00);
                                wca.setPrice(newPrice);
                                productCard.setTotalValue();
                                wca.setTotalValue(Math.abs(productCard.getTotalQuantity() * newPrice - productCard.getTotalValue()));
                                wca.setOrdinalNumber(new Random().nextInt(1000 - 1) + 1);
                                wca.setProductCard(productCard);
                            }
                            cardAnalytic.setDirection("U");
                            cardAnalytic.setTrafficType("PR");
                        } else {
                            if(item.getQuantity()<= product.getProductCard().getTotalQuantity()) {
                                productCard.setTrafficExitQuantity(productCard.getTrafficExitQuantity()+item.getQuantity());
                                productCard.setTrafficExitValue(productCard.getTrafficExitValue()+item.getValue());
                                cardAnalytic.setDirection("I");
                                cardAnalytic.setTrafficType("OT");
                            }else{
                                continue;
                            }
                        }
                        productCard.setTotalQuantity();
                        productCard.setTotalValue();
                        cardAnalytic.setDocumentItem(item);
                        cardAnalytic.setQuantity(item.getQuantity());
                        cardAnalytic.setPrice(item.getPrice());
                        cardAnalytic.setTotalValue(item.getValue());
                        cardAnalytic.setOrdinalNumber(new Random().nextInt(1000 - 1) + 1);
                        cardAnalytic.setProductCard(productCard);
                        warehouseCardAnalyticService.save(cardAnalytic);
                       productCard=productCardService.save(productCard);
                        if(wca != null){
                           warehouseCardAnalyticService.save(wca);
                        }

                         productService.save(product);

               }
           }else{
               return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
           }

        entity.setType(dto.getType());
        entity.setWarehouse(warehouseService.findOne(dto.getWarehouse().getId()));
        entity=documentServiceInterface.save(entity);
        return  new ResponseEntity<>(new DocumentDTO(entity),HttpStatus.OK);
    }


    @DeleteMapping(value = "/{id}")
    public  ResponseEntity<Void> delete(@PathVariable("id") Integer id){
        if(documentServiceInterface.findOne(id) == null)
            return  new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        documentServiceInterface.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
