package ftn.poslovnainformatika.warehouse.controller;

import ftn.poslovnainformatika.warehouse.dto.DocumentItemDTO;
import ftn.poslovnainformatika.warehouse.dto.DocumentItemPrintDTO;
import ftn.poslovnainformatika.warehouse.dto.support.DocumentItemDTOToDocumentItem;
import ftn.poslovnainformatika.warehouse.dto.support.DocumentItemToDocumentItemDTOConverter;
import ftn.poslovnainformatika.warehouse.entity.DocumentItem;
import ftn.poslovnainformatika.warehouse.entity.Product;
import ftn.poslovnainformatika.warehouse.entity.WarehouseCardAnalytic;
import ftn.poslovnainformatika.warehouse.service.DocumentItemServiceInterface;
import ftn.poslovnainformatika.warehouse.service.DocumentServiceInterface;
import ftn.poslovnainformatika.warehouse.service.ProductServiceInterface;
import ftn.poslovnainformatika.warehouse.service.WarehouseCardAnalyticServiceInterface;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@RestController
@RequestMapping(value = "api/documentItems")
@CrossOrigin("*")
public class DocumentItemController {

    @Autowired
    DocumentItemServiceInterface documentItemServiceInterface;

    @Autowired
    DocumentServiceInterface documentService;

    @Autowired
    DocumentItemToDocumentItemDTOConverter toDocumentItemDTO;

    @Autowired
    DocumentItemDTOToDocumentItem toDocumentItem;

    @Autowired
    ProductServiceInterface productService;

    @Autowired
    WarehouseCardAnalyticServiceInterface warehouseCardAnalyticService;

   /* @GetMapping
    public ResponseEntity<List<DocumentItemDTO>> getAll(){
        List<DocumentItem> entity=documentItemServiceInterface.findAll();
        List<DocumentItemDTO> dtos=new ArrayList<>();
        for(DocumentItem documentItem:entity){
            dtos.add(new DocumentItemDTO(documentItem));
        }
        return  new ResponseEntity<>(dtos, HttpStatus.OK);
    }*/

    @GetMapping()
    public ResponseEntity<List<DocumentItemDTO>> getAllByPage(@RequestParam(defaultValue = "0") int pageNum){
        Page<DocumentItem> items = documentItemServiceInterface.findAll(pageNum);

        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages",Integer.toString(items.getTotalPages()));
        headers.add("access-control-expose-headers","totalPages");
        return new ResponseEntity<>(toDocumentItemDTO.convert(items.getContent()),headers,HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<DocumentItemDTO> getById(@PathVariable("id") Integer id){
        if(documentItemServiceInterface.findOne(id) == null)
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return  new ResponseEntity<>(new DocumentItemDTO(documentItemServiceInterface.findOne(id)),HttpStatus.OK);
    }

    @GetMapping(value = "/document/{id}")
    public ResponseEntity<List<DocumentItemPrintDTO>> getByDocument(@PathVariable("id") Integer id){

        if(documentItemServiceInterface.getByDocument(id) == null)
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
        List<DocumentItemPrintDTO> documentItemDTOS=new ArrayList<>();
        for (DocumentItem item:documentItemServiceInterface.getByDocument(id)) {
            DocumentItemPrintDTO dto=new DocumentItemPrintDTO();
            dto.setId(item.getId());
            dto.setValue(item.getValue());
            dto.setQuantity(item.getQuantity());
            dto.setPrice(item.getPrice());
            dto.setArticleId(item.getProduct().getId());
            dto.setArticleName(item.getProduct() .getProductName());
            documentItemDTOS.add(dto);
        }
        return  new ResponseEntity<>(documentItemDTOS,HttpStatus.OK);
    }

    @PostMapping
    public  ResponseEntity<?> create(@Validated @RequestBody DocumentItemDTO dto, BindingResult result){
        if(result.hasErrors()){
            return new ResponseEntity<>(result.toString(), HttpStatus.BAD_REQUEST);
        }
        DocumentItem entity = documentItemServiceInterface.save(toDocumentItem.convert(dto));

        return new ResponseEntity<>(toDocumentItemDTO.convert(entity),HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<DocumentItemDTO> update(@RequestBody DocumentItemDTO dto,@PathVariable("id") Integer id){
        if(documentItemServiceInterface.findOne(id) == null)
            return  new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        DocumentItem entity=documentItemServiceInterface.findOne(id);
        entity=documentItemServiceInterface.save(toDocumentItem.convert(dto));
        return new ResponseEntity<>(toDocumentItemDTO.convert(entity),HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Integer id){
        if(documentItemServiceInterface.findOne(id) == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        documentItemServiceInterface.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
