package ftn.poslovnainformatika.warehouse.controller;

import ftn.poslovnainformatika.warehouse.dto.MeasurementUnitDTO;
import ftn.poslovnainformatika.warehouse.dto.support.MeasurementUnitToMeasurementUnitDTOConverter;
import ftn.poslovnainformatika.warehouse.entity.MeasurementUnit;
import ftn.poslovnainformatika.warehouse.service.MeasurementUnitServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(value = "api/measurementUnit")
public class MeasurementUnitController {

    @Autowired
    private MeasurementUnitServiceInterface measurementUnitService;

    @Autowired
    private MeasurementUnitToMeasurementUnitDTOConverter dtoConverter;


    @GetMapping()
    public ResponseEntity<List<MeasurementUnitDTO>> getAllByPage(@RequestParam(defaultValue = "0") int pageNum){
        List<MeasurementUnit> units = measurementUnitService.findAll();


        return new ResponseEntity<>(dtoConverter.convert(units), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public  ResponseEntity<MeasurementUnitDTO> getById(@PathVariable("id") Integer id){

        if(measurementUnitService.findOne(id) == null)
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return  new ResponseEntity<>(new MeasurementUnitDTO(measurementUnitService.findOne(id)) ,HttpStatus.OK);
    }

    @PostMapping
    public  ResponseEntity<MeasurementUnitDTO> create(@RequestBody MeasurementUnitDTO dto){
        MeasurementUnit entity=new MeasurementUnit();
        entity.setName(dto.getName());
        entity=measurementUnitService.save(entity);

        return  new ResponseEntity<>(new MeasurementUnitDTO(entity),HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<MeasurementUnitDTO> update(@RequestBody MeasurementUnitDTO dto,@PathVariable("id") Integer id){
        if(measurementUnitService.findOne(id) == null)
            return  new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        MeasurementUnit entity=measurementUnitService.findOne(id);
        entity.setName(dto.getName());
        entity=measurementUnitService.save(entity);

        return  new ResponseEntity<>(new MeasurementUnitDTO(entity),HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Integer id){
        if(measurementUnitService.findOne(id) == null)
            return  new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        measurementUnitService.delete(id);
        return  new ResponseEntity<>(HttpStatus.OK);
    }

}
