package ftn.poslovnainformatika.warehouse.controller;

import ftn.poslovnainformatika.warehouse.dto.WarehouseDTO;
import ftn.poslovnainformatika.warehouse.dto.support.WarehouseDTOToWarehouse;
import ftn.poslovnainformatika.warehouse.dto.support.WarehouseToWarehouseDTOConverter;
import ftn.poslovnainformatika.warehouse.entity.Warehouse;
import ftn.poslovnainformatika.warehouse.service.CompanyServiceInterface;
import ftn.poslovnainformatika.warehouse.service.EmployeeServiceInterface;
import ftn.poslovnainformatika.warehouse.service.WarehouseService;
import ftn.poslovnainformatika.warehouse.service.WarehouseServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "api/warehouse")
@CrossOrigin("*")
public class WarehouseController {

    @Autowired
    WarehouseServiceInterface warehouseService;

    @Autowired
    EmployeeServiceInterface employeeService;

    @Autowired
    CompanyServiceInterface companyService;

    @Autowired
    WarehouseToWarehouseDTOConverter toWarehouseDTO;

    @Autowired
    WarehouseDTOToWarehouse toWarehouse;

    /*@GetMapping
    public ResponseEntity<List<WarehouseDTO>> getAll(){
        List<Warehouse> entity= warehouseService.findAll();
        List<WarehouseDTO> dtos=new ArrayList<>();
        for(Warehouse Warehouse:entity){
            dtos.add(new WarehouseDTO(Warehouse));
        }
        return  new ResponseEntity<>(dtos, HttpStatus.OK);
    }*/

    @GetMapping()
    public ResponseEntity<List<WarehouseDTO>> getAllByPage(@RequestParam(defaultValue = "0") int pageNum){
        Page<Warehouse> warehouses = warehouseService.findAll(pageNum);

        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages",Integer.toString(warehouses.getTotalPages()));
        return new ResponseEntity<>(toWarehouseDTO.convert(warehouses.getContent()),headers,HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public  ResponseEntity<WarehouseDTO> getById(@PathVariable("id") Integer id){

        if(warehouseService.findOne(id) == null)
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return  new ResponseEntity<>(new WarehouseDTO(warehouseService.findOne(id)) ,HttpStatus.OK);
    }

    @PostMapping
    public  ResponseEntity<?> create(@Validated @RequestBody WarehouseDTO dto, BindingResult result){
        if(result.hasErrors()){
            return new ResponseEntity<>(result.toString(), HttpStatus.BAD_REQUEST);
        }
        Warehouse entity = warehouseService.save(toWarehouse.convert(dto));

        return  new ResponseEntity<>(toWarehouseDTO.convert(entity),HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<WarehouseDTO> update(@RequestBody WarehouseDTO dto,@PathVariable("id") Integer id){
        if(warehouseService.findOne(id) == null)
            return  new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        Warehouse entity=warehouseService.findOne(id);
        entity=warehouseService.save(toWarehouse.convert(dto));

        return  new ResponseEntity<>(toWarehouseDTO.convert(entity),HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Integer id){
        if(warehouseService.findOne(id) == null)
            return  new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        warehouseService.delete(id);
        return  new ResponseEntity<>(HttpStatus.OK);
    }
}
