package ftn.poslovnainformatika.warehouse.controller;

import ftn.poslovnainformatika.warehouse.dto.BusinessPartnerDTO;
import ftn.poslovnainformatika.warehouse.dto.support.BusinessPartnerDTOToBusinessPartner;
import ftn.poslovnainformatika.warehouse.dto.support.BusinessPartnerToBusinessPartnerDTOConverter;
import ftn.poslovnainformatika.warehouse.entity.BusinessPartner;
import ftn.poslovnainformatika.warehouse.service.BusinessPartnerServiceInterface;
import ftn.poslovnainformatika.warehouse.service.PlaceServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "api/businessPartners")
@CrossOrigin("*")
public class BusinessPartnerController {

    @Autowired
    private BusinessPartnerServiceInterface  businessPartnerService;

    @Autowired
    private PlaceServiceInterface placeService;

    @Autowired
    BusinessPartnerToBusinessPartnerDTOConverter toBusinessPartnerDTO;

    @Autowired
    BusinessPartnerDTOToBusinessPartner toBusinessPartner;

   /* @GetMapping
    public ResponseEntity<List<BusinessPartnerDTO>> getAll(){

        List<BusinessPartner> entity=businessPartnerService.findAll();
        List<BusinessPartnerDTO> dtos=new ArrayList<BusinessPartnerDTO>();
        if(entity != null){
            for (BusinessPartner partner:entity) {
                dtos.add(new BusinessPartnerDTO(partner));
            }
        }
        return  new  ResponseEntity<>(dtos,HttpStatus.OK);
    }*/
   @GetMapping
    public ResponseEntity<List<BusinessPartnerDTO>> getAll(@RequestParam(defaultValue = "0") int pageNum) {

        Page<BusinessPartner> partners = businessPartnerService.findAll(pageNum);

        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages",Integer.toString(partners.getTotalPages()));
       headers.add("access-control-expose-headers","totalPages");

        return new ResponseEntity<>(toBusinessPartnerDTO.convert(partners.getContent()), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public  ResponseEntity<BusinessPartnerDTO>getById(@PathVariable("id")Integer id){
        if(id == 0)
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return  new ResponseEntity<>(new BusinessPartnerDTO(businessPartnerService.findOne(id)),HttpStatus.OK);
    }

    @PostMapping(consumes = "application/json")
    public  ResponseEntity<?> create(@Validated @RequestBody BusinessPartnerDTO businessPartnerDTO, BindingResult result){
        if(result.hasErrors()){
            return new ResponseEntity<>(result.toString(), HttpStatus.BAD_REQUEST);
        }
       BusinessPartner entity = businessPartnerService.save(toBusinessPartner.convert(businessPartnerDTO));

        return new ResponseEntity<>(toBusinessPartnerDTO.convert(entity),HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public  ResponseEntity<BusinessPartnerDTO> update(@RequestBody BusinessPartnerDTO dto,@PathVariable("id")Integer id){
        if(businessPartnerService.findOne(id) == null)
            return  new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        BusinessPartner entity = businessPartnerService.findOne(id);
        entity=businessPartnerService.save(toBusinessPartner.convert(dto));
        return new ResponseEntity<>(toBusinessPartnerDTO.convert(entity),HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Integer id){
        if(businessPartnerService.findOne(id) == null)
            return  new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        businessPartnerService.delete(id);
        return  new ResponseEntity(HttpStatus.OK);
    }
}
