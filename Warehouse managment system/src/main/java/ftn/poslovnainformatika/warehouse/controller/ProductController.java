package ftn.poslovnainformatika.warehouse.controller;

import ftn.poslovnainformatika.warehouse.dto.ProductCategoryDTO;
import ftn.poslovnainformatika.warehouse.dto.ProductDTO;
import ftn.poslovnainformatika.warehouse.dto.support.ProductCardDTOToProductCard;
import ftn.poslovnainformatika.warehouse.dto.support.ProductCardToProductCardDTOConverter;
import ftn.poslovnainformatika.warehouse.dto.support.ProductCategoryToProductCategoryDTOConverter;
import ftn.poslovnainformatika.warehouse.dto.support.ProductToProductDTOConverter;
import ftn.poslovnainformatika.warehouse.entity.*;
import ftn.poslovnainformatika.warehouse.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

@RestController
@RequestMapping(value = "api/products")
@CrossOrigin("*")
public class ProductController {

    @Autowired
    ProductServiceInterface productService;

    @Autowired
    MeasurementUnitServiceInterface measurementUnitService;

    @Autowired
    DocumentItemServiceInterface documentItemService;

    @Autowired
    ProductCategoryServiceInterface productCategoryService;

    @Autowired
    ProductCardServiceIntefrace productCardService;

    @Autowired
    ProductToProductDTOConverter toProductDTO;

    @Autowired
    ProductCardDTOToProductCard toProductCard;

    @Autowired
    BusinessPartnerServiceInterface businessPartnerService;

    @Autowired
    WarehouseCardAnalyticServiceInterface warehouseCardAnalyticService;

    @Autowired
    DocumentServiceInterface documentService;

    @Autowired
    BusinessYearServiceInterface businessYearService;

    /*@GetMapping
    public ResponseEntity<List<ProductDTO>> getAll(){
        List<Product> entity=productService.findAll();
        List<ProductDTO> dtos=new ArrayList<>();
        for(Product Product:entity){
            dtos.add(new ProductDTO(Product));
        }
        return  new ResponseEntity<>(dtos, HttpStatus.OK);
    }*/

    @GetMapping()
    public ResponseEntity<List<ProductDTO>> getAllByPage(@RequestParam(defaultValue = "0") int pageNum){
        Page<Product> products = productService.findAll(pageNum);

        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages",Integer.toString(products.getTotalPages()));
        headers.add("access-control-expose-headers","totalPages");
        return new ResponseEntity<>(toProductDTO.convert(products.getContent()),headers,HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public  ResponseEntity<ProductDTO> getById(@PathVariable("id") Integer id){

        if(productService.findOne(id) == null)
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return  new ResponseEntity<>(new ProductDTO(productService.findOne(id)) ,HttpStatus.OK);
    }

    @GetMapping(value = "/warehouse/{id}/{pageNum}")
    public  ResponseEntity<List<ProductDTO>> getByWarehouse(@PathVariable("id") Integer id,@PathVariable("pageNum") int pageNum){

        if(productService.findByWarehouse(id,pageNum) == null)
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
        List<ProductDTO> productDTOS=new ArrayList<>();
        Page<Product> products=productService.findByWarehouse(id,pageNum);
        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages",Integer.toString(products.getTotalPages()));
        headers.add("access-control-expose-headers","totalPages");
        return  new ResponseEntity<>(toProductDTO.convert(products.getContent()),headers ,HttpStatus.OK);
    }

    @GetMapping(value = "/search")
    public  ResponseEntity<List<ProductDTO>> search(@RequestParam String text,@RequestParam(defaultValue = "0") int pageNum){

        if(productService.search(text,pageNum) == null)
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
        List<ProductDTO> productDTOS=new ArrayList<>();
        Page<Product> products=productService.search(text,pageNum);
        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages",Integer.toString(products.getTotalPages()));
        headers.add("access-control-expose-headers","totalPages");
        return  new ResponseEntity<>(toProductDTO.convert(products.getContent()),headers ,HttpStatus.OK);
    }

    @PostMapping
    public  ResponseEntity<?> create(@Validated @RequestBody ProductDTO dto, @RequestParam Integer businessPartner, BindingResult result){
        if(result.hasErrors()){
            return new ResponseEntity<>(result.toString(), HttpStatus.BAD_REQUEST);
        }
        Product entity=new Product();
        entity.setProductName(dto.getProductName());
        entity.setMeasurementUnit(measurementUnitService.findOne(dto.getMeasurementUnit().getId()));
        entity.setProductCategory(productCategoryService.findOne(dto.getProductCategory().getId()));
        ProductCard productCard=toProductCard.convert(dto.getProductCard());
        productCard.setInitialStateValue(productCard.getInitialStateQuantity()*productCard.getPrice());
        productCard.setTotalValue();
        productCard.setTotalQuantity();
        productCard.setPrice(productCard.getPrice());
        productCard=productCardService.save(productCard);
        entity.setProductCard(productCard);
        entity=productService.save(entity);
        Document document=new Document();
        BusinessYear year=businessYearService.findOne(1);
        document.setCreateDate(new Date());
        document.setBookingDate(new Date());
        document.setSerialNumber(new Random().nextInt(1000-1)+1);
        document.setStatus("Proknjizeno");
        document.setType("Primka");
        document.setWarehouse(productCard.getWarehouse());
        document.setBusinessPartner(businessPartnerService.findOne(businessPartner));
        document.setBusinessYear(year);
        document=documentService.save(document);
        DocumentItem documentItem=new DocumentItem();
        documentItem.setPrice(productCard.getPurchasePrice());
        documentItem.setProduct(entity);
        documentItem.setQuantity(productCard.getInitialStateQuantity());
        documentItem.setValue(productCard.getInitialStateQuantity()*productCard.getPurchasePrice());
        documentItem.setDocument(document);
        documentItem=documentItemService.save(documentItem);
        WarehouseCardAnalytic cardAnalytic=new WarehouseCardAnalytic();
        cardAnalytic.setOrdinalNumber(new Random().nextInt(1000-1)+1);
        cardAnalytic.setDirection("U");
        cardAnalytic.setTrafficType("PS");
        cardAnalytic.setQuantity(productCard.getInitialStateQuantity());
        cardAnalytic.setPrice(productCard.getPurchasePrice());
        cardAnalytic.setTotalValue(productCard.getInitialStateQuantity()*productCard.getPurchasePrice());
        cardAnalytic.setDocumentItem(documentItem);
        cardAnalytic.setProductCard(productCard);
        cardAnalytic=warehouseCardAnalyticService.save(cardAnalytic);
        return  new ResponseEntity<>(new ProductDTO(entity),HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<ProductDTO> update(@RequestBody ProductDTO dto,@PathVariable("id") Integer id){
        if(productService.findOne(id) == null)
            return  new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        Product entity=productService.findOne(id);
        entity.setProductName(dto.getProductName());
        entity.setMeasurementUnit(measurementUnitService.findOne(dto.getMeasurementUnit().getId()));
        entity.setProductCategory(productCategoryService.findOne(dto.getProductCategory().getId()));
        entity.setProductCard(productCardService.findOne(dto.getProductCard().getId()));
        entity=productService.save(entity);

        return  new ResponseEntity<>(new ProductDTO(entity),HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Integer id){
        if(productService.findOne(id) == null)
            return  new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        productService.delete(id);
        return  new ResponseEntity<>(HttpStatus.OK);
    }
}
