package ftn.poslovnainformatika.warehouse.controller;

import ftn.poslovnainformatika.warehouse.dto.ProductCardDTO;
import ftn.poslovnainformatika.warehouse.dto.support.ProductCardDTOToProductCard;
import ftn.poslovnainformatika.warehouse.dto.support.ProductCardToProductCardDTOConverter;
import ftn.poslovnainformatika.warehouse.entity.ProductCard;
import ftn.poslovnainformatika.warehouse.service.BusinessYearServiceInterface;
import ftn.poslovnainformatika.warehouse.service.ProductCardServiceIntefrace;
import ftn.poslovnainformatika.warehouse.service.WarehouseServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "api/productCard")
public class ProductCardController {

    @Autowired
    private ProductCardServiceIntefrace productCardService;

    @Autowired
    private ProductCardToProductCardDTOConverter toProductCardDTO;

    @Autowired
    private ProductCardDTOToProductCard toProductCard;

    @Autowired
    private BusinessYearServiceInterface businessYearService;

    @Autowired
    private WarehouseServiceInterface warehouseService;


    @GetMapping()
    public ResponseEntity<List<ProductCardDTO>> getAllByPage(@RequestParam(defaultValue = "0") int pageNum){
        Page<ProductCard> productCards = productCardService.findAll(pageNum);

        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages",Integer.toString(productCards.getTotalPages()));
        return new ResponseEntity<>(toProductCardDTO.convert(productCards.getContent()),headers, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public  ResponseEntity<ProductCardDTO> getById(@PathVariable("id") Integer id){

        if(productCardService.findOne(id) == null)
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return  new ResponseEntity<>(new ProductCardDTO(productCardService.findOne(id)) ,HttpStatus.OK);
    }


    @GetMapping(value = "/warehouse/{id}")
    public  ResponseEntity<List<ProductCardDTO>> getByWarehouseId(@PathVariable("id") Integer id){

        if(productCardService.getByWarehouse_Id(id) == null)
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
        List<ProductCardDTO> productCardDTOS=new ArrayList<>();
        for (ProductCard card:productCardService.getByWarehouse_Id(id)) {
            productCardDTOS.add(new ProductCardDTO(card));
        }
        return new ResponseEntity<>(productCardDTOS,HttpStatus.OK);
    }

    @PostMapping
    public  ResponseEntity<?> create(@Validated @RequestBody ProductCardDTO dto, BindingResult result){
        if(result.hasErrors()){
            return new ResponseEntity<>(result.toString(), HttpStatus.BAD_REQUEST);
        }
        ProductCard entity = productCardService.save(toProductCard.convert(dto));

        return  new ResponseEntity<>(toProductCardDTO.convert(entity),HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<ProductCardDTO> update(@RequestBody ProductCardDTO dto,@PathVariable("id") Integer id){
        if(productCardService.findOne(id) == null)
            return  new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        ProductCard entity=productCardService.findOne(id);

        entity=productCardService.save(toProductCard.convert(dto));

        return  new ResponseEntity<>(toProductCardDTO.convert(entity),HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Integer id){
        if(productCardService.findOne(id) == null)
            return  new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        productCardService.delete(id);
        return  new ResponseEntity<>(HttpStatus.OK);
    }
}
