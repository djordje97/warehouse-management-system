package ftn.poslovnainformatika.warehouse.controller;

import ftn.poslovnainformatika.warehouse.dto.WarehouseCardAnalyticDTO;
import ftn.poslovnainformatika.warehouse.dto.support.WarehouseCardAnalyticDTOToWarehouseCardAnalytic;
import ftn.poslovnainformatika.warehouse.dto.support.WarehouseCardAnalyticToWarehouseCardAnalyticDTOConverter;
import ftn.poslovnainformatika.warehouse.entity.WarehouseCardAnalytic;
import ftn.poslovnainformatika.warehouse.service.ProductCardServiceIntefrace;
import ftn.poslovnainformatika.warehouse.service.WarehouseCardAnalyticServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "api/warehouseCardAnalytics")
@CrossOrigin("*")
public class WarehouseCardAnalyticController {

    @Autowired
    WarehouseCardAnalyticServiceInterface warehouseCardAnalyticService;

    @Autowired
    ProductCardServiceIntefrace productCardService;

    @Autowired
    WarehouseCardAnalyticToWarehouseCardAnalyticDTOConverter toWarehouseCardAnalyticDTO;

    @Autowired
    WarehouseCardAnalyticDTOToWarehouseCardAnalytic toWarehouseCardAnalytic;

    @GetMapping()
    public ResponseEntity<List<WarehouseCardAnalyticDTO>> getAllByPage(@RequestParam(defaultValue = "0") int pageNum){
        Page<WarehouseCardAnalytic> cardAnalytics = warehouseCardAnalyticService.findAll(pageNum);

        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages",Integer.toString(cardAnalytics.getTotalPages()));
        return new ResponseEntity<>(toWarehouseCardAnalyticDTO.convert(cardAnalytics.getContent()),headers,HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public  ResponseEntity<WarehouseCardAnalyticDTO> getById(@PathVariable("id") Integer id){

        if(warehouseCardAnalyticService.findOne(id) == null)
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return  new ResponseEntity<>(new WarehouseCardAnalyticDTO(warehouseCardAnalyticService.findOne(id)) ,HttpStatus.OK);
    }


    @GetMapping(value = "/productCard/{id}")
    public  ResponseEntity<List<WarehouseCardAnalyticDTO>> getByProductCard(@PathVariable("id") Integer id){
        if(warehouseCardAnalyticService.findByProductCard(id) == null)
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
        List<WarehouseCardAnalyticDTO> warehouseCardAnalyticDTOS=new ArrayList<>();
        for (WarehouseCardAnalytic cardAnalytic:warehouseCardAnalyticService.findByProductCard(id)){
            warehouseCardAnalyticDTOS.add(new WarehouseCardAnalyticDTO(cardAnalytic));
        }
        return  new ResponseEntity<>(warehouseCardAnalyticDTOS ,HttpStatus.OK);
    }
    @PostMapping
    public  ResponseEntity<?> create(@Validated @RequestBody WarehouseCardAnalyticDTO dto, BindingResult result){
        if(result.hasErrors()){
            return new ResponseEntity<>(result.toString(), HttpStatus.BAD_REQUEST);
        }
        WarehouseCardAnalytic entity = warehouseCardAnalyticService.save(toWarehouseCardAnalytic.convert(dto));

        return  new ResponseEntity<>(toWarehouseCardAnalyticDTO.convert(entity),HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<WarehouseCardAnalyticDTO> update(@RequestBody WarehouseCardAnalyticDTO dto,@PathVariable("id") Integer id){
        if(warehouseCardAnalyticService.findOne(id) == null)
            return  new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        WarehouseCardAnalytic entity=warehouseCardAnalyticService.findOne(id);
        entity=warehouseCardAnalyticService.save(toWarehouseCardAnalytic.convert(dto));

        return  new ResponseEntity<>(toWarehouseCardAnalyticDTO.convert(entity),HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Integer id){
        if(warehouseCardAnalyticService.findOne(id) == null)
            return  new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        warehouseCardAnalyticService.delete(id);
        return  new ResponseEntity<>(HttpStatus.OK);
    }
}
