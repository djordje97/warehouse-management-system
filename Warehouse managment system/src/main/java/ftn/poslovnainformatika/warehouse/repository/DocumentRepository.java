package ftn.poslovnainformatika.warehouse.repository;

import ftn.poslovnainformatika.warehouse.entity.Document;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentRepository extends PagingAndSortingRepository<Document, Integer> {

    Document getOne(Integer id);
    Document findAllById(Integer id);
}
