package ftn.poslovnainformatika.warehouse.repository;

import ftn.poslovnainformatika.warehouse.entity.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyRepositoory extends PagingAndSortingRepository<Company,Integer > {
    Company getOne(Integer id);
}
