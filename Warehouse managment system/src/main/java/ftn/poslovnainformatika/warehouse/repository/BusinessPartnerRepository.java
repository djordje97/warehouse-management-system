package ftn.poslovnainformatika.warehouse.repository;


import ftn.poslovnainformatika.warehouse.entity.BusinessPartner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BusinessPartnerRepository extends PagingAndSortingRepository<BusinessPartner,Integer> {
    BusinessPartner getOne(Integer id);
}
