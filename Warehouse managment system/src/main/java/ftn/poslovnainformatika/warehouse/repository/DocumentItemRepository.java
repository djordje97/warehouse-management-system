package ftn.poslovnainformatika.warehouse.repository;

import ftn.poslovnainformatika.warehouse.entity.DocumentItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DocumentItemRepository extends PagingAndSortingRepository<DocumentItem, Integer> {

    DocumentItem getOne(Integer id);
    List<DocumentItem> getByDocument_Id(Integer id);
}
