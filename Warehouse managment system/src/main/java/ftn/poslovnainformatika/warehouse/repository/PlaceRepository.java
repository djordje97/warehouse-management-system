package ftn.poslovnainformatika.warehouse.repository;

import ftn.poslovnainformatika.warehouse.entity.Place;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlaceRepository extends PagingAndSortingRepository<Place,Integer> {

    Place findByName(String name);

    Place getOne(Integer id);
}
