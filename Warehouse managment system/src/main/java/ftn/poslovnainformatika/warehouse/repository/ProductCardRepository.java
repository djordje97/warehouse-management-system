package ftn.poslovnainformatika.warehouse.repository;

import ftn.poslovnainformatika.warehouse.entity.ProductCard;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductCardRepository extends PagingAndSortingRepository<ProductCard,Integer> {
    ProductCard getOne(Integer id);

    List<ProductCard> getByWarehouse_Id(Integer id);
}
