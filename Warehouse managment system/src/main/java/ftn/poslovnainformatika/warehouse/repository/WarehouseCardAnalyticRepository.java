package ftn.poslovnainformatika.warehouse.repository;

import ftn.poslovnainformatika.warehouse.entity.WarehouseCardAnalytic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WarehouseCardAnalyticRepository extends PagingAndSortingRepository<WarehouseCardAnalytic,Integer> {
    WarehouseCardAnalytic getOne(Integer id);

    List<WarehouseCardAnalytic> getByProductCard_Id(Integer id);
}
