package ftn.poslovnainformatika.warehouse.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "company")
public class Company {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",nullable = false)
    private Integer id;

    @Column(name = "name",nullable = false)
    private String  name;

    @Column(name = "pib",nullable = false)
    private String  PIB;

    @ManyToOne
    @JoinColumn(name = "placeId",referencedColumnName = "id",nullable = false)
    private Place place;

    @ManyToOne
    @JoinColumn(name = "employeeId",referencedColumnName = "id",nullable = false)
    private Employee employee;

    @OneToMany(cascade = {CascadeType.ALL},fetch = FetchType.LAZY,mappedBy = "company")
    private Set<Warehouse> warehouses = new HashSet<>();

    public Company(){}

    public Company(Integer id, String name, String PIB, Place place, Employee employee, Set<Warehouse> warehouses) {
        this.id = id;
        this.name = name;
        this.PIB = PIB;
        this.place = place;
        this.employee = employee;
        this.warehouses = warehouses;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPIB() {
        return PIB;
    }

    public void setPIB(String PIB) {
        this.PIB = PIB;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Set<Warehouse> getWarehouses() {
        return warehouses;
    }

    public void setWarehouses(Set<Warehouse> warehouses) {
        this.warehouses = warehouses;
    }
}
