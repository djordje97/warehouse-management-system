package ftn.poslovnainformatika.warehouse.entity;

import javax.persistence.*;

@Entity
@Table(name = "warehouseCardAnalytic")
public class WarehouseCardAnalytic {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",nullable = false)
    private Integer id;

    @Column(name = "ordinalNumber",nullable = false)
    private Integer ordinalNumber;

    @Column(name = "trafficType",nullable = false)
    private String trafficType;

    @Column(name = "direction",nullable = false)
    private String direction;

    @Column(name = "quantity",nullable = false)
    private double quantity;

    @Column(name = "price",nullable = false)
    private double price;

    @Column(name = "totalValue",nullable = false)
    private double totalValue;

    @ManyToOne
    @JoinColumn(name = "productCardId",referencedColumnName = "id",nullable = false)
    private ProductCard productCard;

    @ManyToOne
    @JoinColumn(name = "documentItemId",referencedColumnName = "id",nullable = false)
    private DocumentItem documentItem;

    public WarehouseCardAnalytic() {}

    public WarehouseCardAnalytic(Integer ordinalNumber, String trafficType, String direction, double quantity, double price, double totalValue, ProductCard productCard, DocumentItem documentItem) {
        this.ordinalNumber = ordinalNumber;
        this.trafficType = trafficType;
        this.direction = direction;
        this.quantity = quantity;
        this.price = price;
        this.totalValue = totalValue;
        this.productCard = productCard;
        this.documentItem = documentItem;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrdinalNumber() {
        return ordinalNumber;
    }

    public void setOrdinalNumber(Integer ordinalNumber) {
        this.ordinalNumber = ordinalNumber;
    }

    public String getTrafficType() {
        return trafficType;
    }

    public void setTrafficType(String trafficType) {
        this.trafficType = trafficType;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(double totalValue) {
        this.totalValue = totalValue;
    }

    public ProductCard getProductCard() {
        return productCard;
    }

    public void setProductCard(ProductCard productCard) {
        this.productCard = productCard;
    }

    public DocumentItem getDocumentItem() {
        return documentItem;
    }

    public void setDocumentItem(DocumentItem documentItem) {
        this.documentItem = documentItem;
    }
}
