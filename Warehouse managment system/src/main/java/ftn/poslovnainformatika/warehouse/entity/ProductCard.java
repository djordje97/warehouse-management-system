package ftn.poslovnainformatika.warehouse.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "productCard")
public class ProductCard {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",nullable = false)
    private Integer id;
    @Column(name = "initialStateQuantity",nullable = false)
    private double initialStateQuantity;
    @Column(name = "initialStateValue",nullable = false)
    private double initialStateValue;
    @Column(name = "trafficEntryQuantity",nullable = false)
    private double trafficEntryQuantity;
    @Column(name = "trafficEntryValue",nullable = false)
    private  double trafficEntryValue;
    @Column(name = "trafficExitQuantity",nullable = false)
    private double trafficExitQuantity;
    @Column(name = "trafficExitValue",nullable = false)
    private  double trafficExitValue;
    @Column(name = "totalQuantity",nullable = false)
    private double totalQuantity;
    @Column(name = "totalValue",nullable = false)
    private double totalValue;
    @Column(name ="price" ,nullable = false)
    private double price;
    @Column(name ="purchasePrice",nullable = false)
    private double purchasePrice;
    @ManyToOne
    @JoinColumn(name = "businessYearId",referencedColumnName = "id",nullable = false)
    private BusinessYear businessYear;
    @ManyToOne
    @JoinColumn(name = "warehouseId",referencedColumnName = "id",nullable = false)
    private Warehouse warehouse;
    @OneToMany(cascade = {CascadeType.ALL},fetch = FetchType.LAZY,mappedBy = "productCard")
    private Set<Product> products = new HashSet<>();
    @OneToMany(cascade = {CascadeType.ALL},fetch = FetchType.LAZY,mappedBy = "productCard")
    private  Set<WarehouseCardAnalytic> warehouseCardAnalytics=new HashSet<>();

    public ProductCard() {}

    public ProductCard(double initialStateQuantity, double initialStateValue, double trafficEntryQuantity, double trafficEntryValue, double trafficExitQuantity, double trafficExitValue, double totalQuantity, double totalValue, double price, double purchasePrice, BusinessYear businessYear, Warehouse warehouse, Set<Product> products, Set<WarehouseCardAnalytic> warehouseCardAnalytics) {
        this.initialStateQuantity = initialStateQuantity;
        this.initialStateValue = initialStateValue;
        this.trafficEntryQuantity = trafficEntryQuantity;
        this.trafficEntryValue = trafficEntryValue;
        this.trafficExitQuantity = trafficExitQuantity;
        this.trafficExitValue = trafficExitValue;
        this.totalQuantity = totalQuantity;
        this.totalValue = totalValue;
        this.price = price;
        this.purchasePrice = purchasePrice;
        this.businessYear = businessYear;
        this.warehouse = warehouse;
        this.products = products;
        this.warehouseCardAnalytics = warehouseCardAnalytics;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getInitialStateQuantity() {
        return initialStateQuantity;
    }

    public void setInitialStateQuantity(double initialStateQuantity) {
        this.initialStateQuantity = initialStateQuantity;
    }

    public double getInitialStateValue() {
        return initialStateValue;
    }

    public void setInitialStateValue(double initialStateValue) {
        this.initialStateValue = initialStateValue;
    }

    public double getTrafficEntryQuantity() {
        return trafficEntryQuantity;
    }

    public void setTrafficEntryQuantity(double trafficEntryQuantity) {
        this.trafficEntryQuantity = trafficEntryQuantity;
    }

    public double getTrafficEntryValue() {
        return trafficEntryValue;
    }

    public void setTrafficEntryValue(double trafficEntryValue) {
        this.trafficEntryValue = trafficEntryValue;
    }

    public double getTrafficExitQuantity() {
        return trafficExitQuantity;
    }

    public void setTrafficExitQuantity(double trafficExitQuantity) {
        this.trafficExitQuantity = trafficExitQuantity;
    }

    public double getTrafficExitValue() {
        return trafficExitValue;
    }

    public void setTrafficExitValue(double trafficExitValue) {
        this.trafficExitValue = trafficExitValue;
    }

    public double getTotalQuantity() {
        return getCalculateTotalQuantity();
    }

    public void setTotalQuantity() {

        this.totalQuantity = getCalculateTotalQuantity();
    }

    public double getTotalValue() {
        return getCalculateTotalValue();
    }

    public void setTotalValue() {
        this.totalValue = getCalculateTotalValue();
    }

    public BusinessYear getBusinessYear() {
        return businessYear;
    }

    public void setBusinessYear(BusinessYear businessYear) {
        this.businessYear = businessYear;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Set<WarehouseCardAnalytic> getWarehouseCardAnalytics() {
        return warehouseCardAnalytics;
    }

    public void setWarehouseCardAnalytics(Set<WarehouseCardAnalytic> warehouseCardAnalytics) {
        this.warehouseCardAnalytics = warehouseCardAnalytics;
    }

    public double getPurchasePrice() {
        return purchasePrice;
    }


    public void setPurchasePrice(double purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public  double getCalculatePrice(){
        double calculatedPrice=0.0;
        calculatedPrice=(this.totalValue+this.trafficEntryQuantity*this.purchasePrice)/(this.totalQuantity+this.trafficEntryQuantity);
        return  calculatedPrice;
    }

    public double getCalculateTotalQuantity(){
        double calculateTotalQuantity = 0.0;
        calculateTotalQuantity = (this.initialStateQuantity + this.trafficEntryQuantity) - this.trafficExitQuantity;
        return calculateTotalQuantity;
    }


    public double getCalculateTotalValue(){
        double calculateTotalValue = 0.0;
        calculateTotalValue = (this.initialStateValue + this.trafficEntryValue) - this.trafficExitValue;
        return calculateTotalValue;
    }
}
