package ftn.poslovnainformatika.warehouse.entity;

import javax.persistence.*;

@Entity
@Table(name = "warehouse")
public class Warehouse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",nullable = false)
    private Integer id;
    @Column(name = "name",nullable = false)
    private String name;
    @ManyToOne
    @JoinColumn(name = "companyId",referencedColumnName = "id",nullable = false)
    private Company company;
    @ManyToOne
    @JoinColumn(name = "employeeId",referencedColumnName = "id",nullable = false)
    private Employee employee;

    public Warehouse() {
    }

    public Warehouse(Integer id, String name, Company company, Employee employee) {
        this.id = id;
        this.name = name;
        this.company = company;
        this.employee = employee;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}
