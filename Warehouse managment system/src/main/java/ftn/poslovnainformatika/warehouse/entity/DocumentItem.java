package ftn.poslovnainformatika.warehouse.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "documentItem")
public class DocumentItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",nullable = false)
    private Integer id;

    @Column(name = "quantity",nullable = false)
    private double quantity;

    @Column(name = "price",nullable = false)
    private double price;

    @Column(name = "value",nullable = false)
    private double value;

    @ManyToOne
    @JoinColumn(name = "documentId",referencedColumnName = "id",nullable = false)
    private Document document;

    @ManyToOne
    @JoinColumn(name = "productId",referencedColumnName = "id",nullable = false)
    private Product product;

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "documentItem")
    private  Set<WarehouseCardAnalytic> warehouseCardAnalytics=new HashSet<>();

    public DocumentItem(){

    }

    public DocumentItem(double quantity, double price, double value, Document document, Product product, Set<WarehouseCardAnalytic> warehouseCardAnalytics) {
        this.quantity = quantity;
        this.price = price;
        this.value = value;
        this.document = document;
        this.product = product;
        this.warehouseCardAnalytics = warehouseCardAnalytics;
    }

    public DocumentItem(DocumentItem item){
        this(item.getQuantity(),item.getPrice(),item.getValue(),item.getDocument(),item.getProduct(),item.getWarehouseCardAnalytics());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {

        this.value = getCalculateValue();
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Set<WarehouseCardAnalytic> getWarehouseCardAnalytics() {
        return warehouseCardAnalytics;
    }

    public void setWarehouseCardAnalytics(Set<WarehouseCardAnalytic> warehouseCardAnalytics) {
        this.warehouseCardAnalytics = warehouseCardAnalytics;
    }

    public double getCalculateValue() {
        double calculateValue = 0.0;
        calculateValue = this.price * this.quantity;
        return calculateValue;
    }

}
