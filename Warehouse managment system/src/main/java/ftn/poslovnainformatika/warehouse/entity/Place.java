package ftn.poslovnainformatika.warehouse.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "place")
public class Place {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",nullable = false)
    private Integer id;

    @Column(name = "name",nullable = false)
    private String name;

    @Column(name = "zipCode",nullable = false)
    private Integer zipCode;

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "place")
    private Set<Company> companies=new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "place")
    private Set<BusinessPartner> businessPartners=new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "place")
    private Set<Employee> employees=new HashSet<>();

    public Place() {}

    public Place(String name, Integer zipCode, Set<Company> companies, Set<BusinessPartner> businessPartners, Set<Employee> employees) {
        this.name = name;
        this.zipCode = zipCode;
        this.companies = companies;
        this.businessPartners = businessPartners;
        this.employees = employees;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getZipCode() {
        return zipCode;
    }

    public void setZipCode(Integer zipCode) {
        this.zipCode = zipCode;
    }

    public Set<Company> getCompanies() {
        return companies;
    }

    public void setCompanies(Set<Company> companies) {
        this.companies = companies;
    }

    public Set<BusinessPartner> getBusinessPartners() {
        return businessPartners;
    }

    public void setBusinessPartners(Set<BusinessPartner> businessPartners) {
        this.businessPartners = businessPartners;
    }

    public Set<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(Set<Employee> employees) {
        this.employees = employees;
    }
}
