package ftn.poslovnainformatika.warehouse.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",nullable = false)
    private Integer id;

    @Column(name = "productName",nullable = false)
    private String productName;

    @ManyToOne
    @JoinColumn(name = "measurementUnitId",referencedColumnName = "id",nullable = false)
    private MeasurementUnit measurementUnit;

    @OneToMany(mappedBy = "product",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private Set<DocumentItem> documentItems=new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "productCardId",referencedColumnName = "id",nullable = false)
    private ProductCard productCard;

    @ManyToOne
    @JoinColumn(name = "productCategoryId",referencedColumnName = "id",nullable = false)
    private ProductCategory productCategory;

    public Product() {}

    public Product(String productName, MeasurementUnit measurementUnit, Set<DocumentItem> documentItems, ProductCard productCard, ProductCategory productCategory) {
        this.productName = productName;
        this.measurementUnit = measurementUnit;
        this.documentItems = documentItems;
        this.productCard = productCard;
        this.productCategory = productCategory;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public MeasurementUnit getMeasurementUnit() {
        return measurementUnit;
    }

    public void setMeasurementUnit(MeasurementUnit measurementUnit) {
        this.measurementUnit = measurementUnit;
    }

    public Set<DocumentItem> getDocumentItems() {
        return documentItems;
    }

    public void setDocumentItems(Set<DocumentItem> documentItems) {
        this.documentItems = documentItems;
    }

    public ProductCard getProductCard() {
        return productCard;
    }

    public void setProductCard(ProductCard productCard) {
        this.productCard = productCard;
    }

    public ProductCategory getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
    }
}
