package ftn.poslovnainformatika.warehouse.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "document")
public class Document implements  Cloneable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",nullable = false)
    private Integer id;

    @Column(name = "serialNumber",nullable = false)
    private Integer serialNumber;

    @Column(name = "createDate",nullable = false)
    private Date createDate;

    @Column(name = "bookingDate",nullable = true)
    private Date bookingDate;

    @Column(name = "status",nullable = false)
    private String status;

    @Column(name = "type",nullable = false)
    private String type;

    @ManyToOne
    @JoinColumn(name = "businessPartnerId",referencedColumnName = "id",nullable = false)
    private BusinessPartner businessPartner;

    @ManyToOne
    @JoinColumn(name = "businessYearId",referencedColumnName = "id",nullable = false)
    private BusinessYear businessYear;

    @ManyToOne
    @JoinColumn(name = "warehouseId",referencedColumnName = "id",nullable = false)
    private Warehouse warehouse;

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "document")
    private Set<DocumentItem> documentItems=new HashSet<>();

    public Document(){

    }

    public Document(Integer serialNumber, Date createDate, Date bookingDate, String status, String type, BusinessPartner businessPartner, BusinessYear businessYear, Warehouse warehouse, Set<DocumentItem> documentItems) {
        this.serialNumber = serialNumber;
        this.createDate = createDate;
        this.bookingDate = bookingDate;
        this.status = status;
        this.type = type;
        this.businessPartner = businessPartner;
        this.businessYear = businessYear;
        this.warehouse = warehouse;
        this.documentItems = documentItems;
    }

    public Document(Document document){
    this(document.getSerialNumber(),document.getCreateDate(),document.getBookingDate(),document.getStatus(),document.getType(),document.getBusinessPartner(),document.getBusinessYear(),document.getWarehouse(),document.getDocumentItems());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(Integer serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(Date bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BusinessPartner getBusinessPartner() {
        return businessPartner;
    }

    public void setBusinessPartner(BusinessPartner businessPartner) {
        this.businessPartner = businessPartner;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public Set<DocumentItem> getDocumentItems() {
        return documentItems;
    }

    public void setDocumentItems(Set<DocumentItem> documentItems) {
        this.documentItems = documentItems;
    }

    public BusinessYear getBusinessYear() {
        return businessYear;
    }

    public void setBusinessYear(BusinessYear businessYear) {
        this.businessYear = businessYear;
    }

    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
