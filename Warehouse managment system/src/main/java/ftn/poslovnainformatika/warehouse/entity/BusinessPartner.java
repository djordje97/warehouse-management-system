package ftn.poslovnainformatika.warehouse.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "businessPartner")
public class BusinessPartner {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",nullable = false)
    private Integer id;

    @Column(name = "name",nullable = false)
    private String name;

    @Column(name = "pib",nullable = false)
    private String pib;
    @Column(name = "address" ,nullable = false)
    private String address;
    @Column(name = "type",nullable = false)
    private String partnerType;
    @ManyToOne
    @JoinColumn(name = "placeId",referencedColumnName = "id",nullable = false)
    private Place place;

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "businessPartner")
    private Set<Document> documents=new HashSet<>();
    public BusinessPartner() {
    }

    public BusinessPartner(String name, String pib, String address, String partnerType, Place place, Set<Document> documents) {
        this.name = name;
        this.pib = pib;
        this.address = address;
        this.partnerType = partnerType;
        this.place = place;
        this.documents = documents;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPib() {
        return pib;
    }

    public void setPib(String pib) {
        this.pib = pib;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(String partnerType) {
        this.partnerType = partnerType;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public Set<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(Set<Document> documents) {
        this.documents = documents;
    }
}
