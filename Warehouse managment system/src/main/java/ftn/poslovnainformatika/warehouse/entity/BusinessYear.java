package ftn.poslovnainformatika.warehouse.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "businessYear")
public class BusinessYear {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",nullable = false)
    private Integer id;
    @Column(name = "year",nullable = false)
    private int  year;
    @Column(name = "isClosed",nullable = false)
    private boolean isClosed;

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "businessYear")
    private Set<ProductCard>productCards=new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "businessYear")
    private  Set<Document>  documents=new HashSet<>();


    public BusinessYear() {
    }

    public BusinessYear(int year, boolean isClosed, Set<ProductCard> productCards, Set<Document> documents) {
        this.year = year;
        this.isClosed = isClosed;
        this.productCards = productCards;
        this.documents = documents;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public boolean isClosed() {
        return isClosed;
    }

    public void setClosed(boolean closed) {
        isClosed = closed;
    }

    public Set<ProductCard> getProductCards() {
        return productCards;
    }

    public void setProductCards(Set<ProductCard> productCards) {
        this.productCards = productCards;
    }

    public Set<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(Set<Document> documents) {
        this.documents = documents;
    }
}
