package ftn.poslovnainformatika.warehouse.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Table
@Entity(name = "productCategory")
public class ProductCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",unique = true,nullable = false)
    private Integer id;

    @Column(name = "productCategoryName",nullable = false)
    private String productCategoryName;

    @OneToMany(cascade = {CascadeType.ALL},fetch = FetchType.LAZY,mappedBy = "productCategory")
    private Set<Product> products = new HashSet<>();

    public ProductCategory(Integer id, String productCategoryName, Set<Product> products) {
        this.id = id;
        this.productCategoryName = productCategoryName;
        this.products = products;
    }

    public ProductCategory() {}

    public void add(Product product){
        if(product.getProductCategory() != null){
            product.getProductCategory().getProducts().remove(product);
        }
        product.setProductCategory(this);
        getProducts().add(product);
    }

    public void remove(Product product){
        product.setProductCategory(null);
        getProducts().remove(product);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProductCategoryName() {
        return productCategoryName;
    }

    public void setProductCategoryName(String productCategoryName) {
        this.productCategoryName = productCategoryName;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }
}
