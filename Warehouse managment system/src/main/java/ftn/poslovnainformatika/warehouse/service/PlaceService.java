package ftn.poslovnainformatika.warehouse.service;

import ftn.poslovnainformatika.warehouse.entity.Place;
import ftn.poslovnainformatika.warehouse.repository.PlaceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlaceService implements PlaceServiceInterface {

    @Autowired
    PlaceRepository placeRepository;


    /*@Override
    public List<Place> findAll() {
        return placeRepository.findAll();
    }*/

    @Override
    public Page<Place> findAll(int pageNum) {
        return placeRepository.findAll(PageRequest.of(pageNum,10));
    }

    @Override
    public Place findOne(Integer id) {
        return placeRepository.getOne(id);
    }

    @Override
    public Place save(Place place) {
        return placeRepository.save(place);
    }

    @Override
    public void delete(Integer id) {
        placeRepository.deleteById(id);
    }

    @Override
    public Place findByName(String name) {
        return placeRepository.findByName(name);
    }
}
