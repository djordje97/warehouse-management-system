package ftn.poslovnainformatika.warehouse.service;
import  ftn.poslovnainformatika.warehouse.entity.BusinessPartner;
import org.springframework.data.domain.Page;

import java.util.List;

public interface BusinessPartnerServiceInterface {

    //List<BusinessPartner> findAll();

    Page<BusinessPartner> findAll(int pageNum);

    BusinessPartner findOne(Integer id);

    BusinessPartner save(BusinessPartner entity);

    void delete(Integer id);

}
