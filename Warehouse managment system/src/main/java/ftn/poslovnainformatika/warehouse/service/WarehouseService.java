package ftn.poslovnainformatika.warehouse.service;

import ftn.poslovnainformatika.warehouse.entity.Warehouse;
import ftn.poslovnainformatika.warehouse.repository.WarehouseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WarehouseService  implements  WarehouseServiceInterface{

    @Autowired
    private WarehouseRepository warehouseRepository;
    /*@Override
    public List<Warehouse> findAll() {
        return warehouseRepository.findAll();
    }*/

    @Override
    public Page<Warehouse> findAll(int pageNum) {
        return warehouseRepository.findAll(PageRequest.of(pageNum,10));
    }

    @Override
    public Warehouse findByName(String name) {
        return warehouseRepository.findByName(name);
    }

    @Override
    public Warehouse findOne(Integer id) {
        return warehouseRepository.getOne(id);
    }

    @Override
    public Warehouse save(Warehouse entity) {
        return warehouseRepository.save(entity);
    }

    @Override
    public void delete(Integer id) {
        warehouseRepository.deleteById(id);
    }
}
