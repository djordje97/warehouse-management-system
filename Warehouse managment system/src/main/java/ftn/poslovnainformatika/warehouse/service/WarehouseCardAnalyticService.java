package ftn.poslovnainformatika.warehouse.service;

import ftn.poslovnainformatika.warehouse.entity.WarehouseCardAnalytic;
import ftn.poslovnainformatika.warehouse.repository.WarehouseCardAnalyticRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WarehouseCardAnalyticService implements WarehouseCardAnalyticServiceInterface {

    @Autowired
    WarehouseCardAnalyticRepository warehouseCardAnalyticRepository;

    /*@Override
    public List<WarehouseCardAnalytic> findAll() {
        return warehouseCardAnalyticRepository.findAll();
    }*/

    @Override
    public Page<WarehouseCardAnalytic> findAll(int pageNum) {
        return warehouseCardAnalyticRepository.findAll(PageRequest.of(pageNum,10));
    }

    @Override
    public WarehouseCardAnalytic findOne(Integer id) {
        return warehouseCardAnalyticRepository.getOne(id);
    }

    @Override
    public WarehouseCardAnalytic save(WarehouseCardAnalytic warehouseCardAnalytic) {
        return warehouseCardAnalyticRepository.save(warehouseCardAnalytic);
    }

    @Override
    public void delete(Integer id) {
        warehouseCardAnalyticRepository.deleteById(id);
    }

    @Override
    public List<WarehouseCardAnalytic> findByProductCard(Integer id) {
        return  warehouseCardAnalyticRepository.getByProductCard_Id(id);
    }
}
