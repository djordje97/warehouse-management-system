package ftn.poslovnainformatika.warehouse.service;

import ftn.poslovnainformatika.warehouse.entity.Document;
import ftn.poslovnainformatika.warehouse.repository.DocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DocumentService implements DocumentServiceInterface{

    @Autowired
    private DocumentRepository documentRepository;



    /*@Override
    public List<Document> findAll(){
        return documentRepository.findAll();
    }*/


    @Override
    public Page<Document> findAll(int pageNum) {
        return documentRepository.findAll(new PageRequest(pageNum,2));
    }

    @Override
    public Document findOne(Integer id){
        return documentRepository.findAllById(id);
    }

    @Override
    public Document save(Document document){
        return documentRepository.save(document);
    }

    @Override
    public void delete(Integer id){
        documentRepository.deleteById(id);
    }



}
