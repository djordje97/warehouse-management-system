package ftn.poslovnainformatika.warehouse.service;

import ftn.poslovnainformatika.warehouse.entity.ProductCategory;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ProductCategoryServiceInterface {

    //List<ProductCategory> findAll();

    Page<ProductCategory> findAll(int pageNum);

    ProductCategory findOne(Integer id);

    ProductCategory save(ProductCategory productCategory);

    void delete(Integer id);
}
