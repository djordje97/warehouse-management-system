package ftn.poslovnainformatika.warehouse.service;

import ftn.poslovnainformatika.warehouse.entity.MeasurementUnit;
import ftn.poslovnainformatika.warehouse.repository.MeasurementUnitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MeasurementUnitService implements MeasurementUnitServiceInterface {

    @Autowired
    MeasurementUnitRepository measurementUnitRepository;

    @Override
    public List<MeasurementUnit> findAll() {
        return measurementUnitRepository.findAll();
    }

    @Override
    public MeasurementUnit findOne(Integer id) {
        return measurementUnitRepository.getOne(id);
    }

    @Override
    public MeasurementUnit save(MeasurementUnit measurementUnit) {
        return measurementUnitRepository.save(measurementUnit);
    }

    @Override
    public void delete(Integer id) {
        measurementUnitRepository.deleteById(id);
    }
}
