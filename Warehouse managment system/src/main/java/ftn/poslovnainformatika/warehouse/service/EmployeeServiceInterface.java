package ftn.poslovnainformatika.warehouse.service;

import ftn.poslovnainformatika.warehouse.entity.Employee;
import org.springframework.data.domain.Page;

import java.util.List;

public interface EmployeeServiceInterface {

    //List<Employee> findAll();

    Page<Employee> findAll(int pageNum);

    Employee findOne(Integer id);

    Employee save(Employee employe);

    void delete(Integer id);

    Employee findByUsername(String username);
}
