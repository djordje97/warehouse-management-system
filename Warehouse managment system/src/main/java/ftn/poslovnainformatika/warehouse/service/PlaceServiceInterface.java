package ftn.poslovnainformatika.warehouse.service;

import ftn.poslovnainformatika.warehouse.entity.Place;
import org.springframework.data.domain.Page;

import java.util.List;

public interface PlaceServiceInterface {

    //List<Place> findAll();

    Page<Place> findAll(int pageNum);


    Place findOne(Integer id);

    Place findByName(String name);

    Place save(Place place);

    void delete(Integer id);
}
