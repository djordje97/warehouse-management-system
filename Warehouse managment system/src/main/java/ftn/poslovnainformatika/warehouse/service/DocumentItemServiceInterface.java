package ftn.poslovnainformatika.warehouse.service;

import ftn.poslovnainformatika.warehouse.entity.DocumentItem;
import org.springframework.data.domain.Page;

import java.util.List;

public interface DocumentItemServiceInterface {

    //List<DocumentItem> findAll();

    Page<DocumentItem> findAll(int pageNum);

    List<DocumentItem> getByDocument(int id);

    DocumentItem findOne(Integer id);

    DocumentItem save(DocumentItem documentItem);

    void delete(Integer id);

}
