package ftn.poslovnainformatika.warehouse.service;

import ftn.poslovnainformatika.warehouse.entity.Warehouse;
import org.springframework.data.domain.Page;

import java.util.List;

public interface WarehouseServiceInterface  {

    //List<Warehouse> findAll();

    Page<Warehouse> findAll(int pageNum);

    Warehouse findByName(String name);

    Warehouse findOne(Integer id);

    Warehouse save(Warehouse entity);

    void delete(Integer id);

}
