package ftn.poslovnainformatika.warehouse.service;

import ftn.poslovnainformatika.warehouse.entity.BusinessYear;
import org.springframework.data.domain.Page;

import java.util.List;

public interface BusinessYearServiceInterface {

    //List<BusinessYear> findAll();

    Page<BusinessYear> findAll(int pageNum);

    BusinessYear findOne(Integer id);

    BusinessYear save(BusinessYear entity);

    void delete(Integer id);
}
