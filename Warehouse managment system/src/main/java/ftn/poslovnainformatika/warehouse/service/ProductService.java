package ftn.poslovnainformatika.warehouse.service;

import ftn.poslovnainformatika.warehouse.entity.Product;
import ftn.poslovnainformatika.warehouse.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService implements ProductServiceInterface {

    @Autowired
    ProductRepository productRepository;

    /*@Override
    public List<Product> findAll() {
        return productRepository.findAll();
    }*/


    @Override
    public Page<Product> findAll(int pageNum) {
        return productRepository.findAll(PageRequest.of(pageNum,3));
    }

    @Override
    public Product findOne(Integer id) {
        return productRepository.getOne(id);
    }

    @Override
    public Product save(Product product) {
        return productRepository.save(product);
    }

    @Override
    public void delete(Integer id) {
        productRepository.deleteById(id);
    }

    @Override
    public Page<Product> findByWarehouse(Integer id,int pageNum) {
        return productRepository.findByProductCard_Warehouse_Id(id,PageRequest.of(pageNum,10));
    }

    @Override
    public Page<Product> search(String text, int pageNum) {
        return productRepository.findByProductName(text,PageRequest.of(pageNum,10));
    }
}
