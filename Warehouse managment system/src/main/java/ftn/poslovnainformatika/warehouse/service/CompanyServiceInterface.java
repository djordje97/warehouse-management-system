package ftn.poslovnainformatika.warehouse.service;

import ftn.poslovnainformatika.warehouse.entity.Company;
import org.springframework.data.domain.Page;

import java.util.List;

public interface CompanyServiceInterface  {

    //List<Company> findAll();

    Page<Company> findAll(int pageNum);

    Company findOne(Integer id);

    Company save(Company company);

    void delete(Integer id);
}
