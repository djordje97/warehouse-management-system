package ftn.poslovnainformatika.warehouse.service;

import ftn.poslovnainformatika.warehouse.entity.DocumentItem;
import ftn.poslovnainformatika.warehouse.repository.DocumentItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DocumentItemService implements DocumentItemServiceInterface {

    @Autowired
    private DocumentItemRepository documentItemRepository;

    /*@Override
    public List<DocumentItem> findAll(){
        return documentItemRepository.findAll();
    }*/

    @Override
    public Page<DocumentItem> findAll(int pageNum) {
        return documentItemRepository.findAll(PageRequest.of(pageNum,10));
    }

    @Override
    public DocumentItem findOne(Integer id){
        return documentItemRepository.getOne(id);
    }

    @Override
    public DocumentItem save(DocumentItem item){
        return documentItemRepository.save(item);
    }

    @Override
    public void delete(Integer id){
        documentItemRepository.deleteById(id);
    }

    @Override
    public List<DocumentItem> getByDocument(int id) {
        return documentItemRepository.getByDocument_Id(id);
    }
}
