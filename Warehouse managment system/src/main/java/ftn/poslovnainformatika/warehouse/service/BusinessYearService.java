package ftn.poslovnainformatika.warehouse.service;

import ftn.poslovnainformatika.warehouse.entity.BusinessYear;
import ftn.poslovnainformatika.warehouse.repository.BusinessYearRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BusinessYearService implements BusinessYearServiceInterface {

    @Autowired
    private BusinessYearRepository repository;

    /*@Override
    public List<BusinessYear> findAll() {
        return repository.findAll();
    }*/

    @Override
    public Page<BusinessYear> findAll(int pageNum) {
        return repository.findAll(new PageRequest(pageNum,10));
    }

    @Override
    public BusinessYear findOne(Integer id) {
        return repository.getOne(id);
    }

    @Override
    public BusinessYear save(BusinessYear entity) {
        return repository.save(entity);
    }

    @Override
    public void delete(Integer id) {
        repository.deleteById(id);
    }
}
