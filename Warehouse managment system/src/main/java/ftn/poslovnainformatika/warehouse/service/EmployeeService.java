package ftn.poslovnainformatika.warehouse.service;

import ftn.poslovnainformatika.warehouse.entity.Employee;
import ftn.poslovnainformatika.warehouse.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService implements EmployeeServiceInterface {

    @Autowired
    private EmployeeRepository employeeRepository;

    /*@Override
    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }*/

    @Override
    public Page<Employee> findAll(int pageNum) {
        return employeeRepository.findAll(PageRequest.of(pageNum,10));
    }

    @Override
    public Employee findOne(Integer id) {
        return employeeRepository.getOne(id);
    }

    @Override
    public Employee save(Employee employe) {
        return employeeRepository.save(employe);
    }

    @Override
    public void delete(Integer id) {
        employeeRepository.deleteById(id);
    }

    @Override
    public Employee findByUsername(String username) {
        return employeeRepository.findByUsername(username);
    }
}
