package ftn.poslovnainformatika.warehouse.service;

import ftn.poslovnainformatika.warehouse.entity.ProductCard;
import ftn.poslovnainformatika.warehouse.repository.ProductCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductCardService implements ProductCardServiceIntefrace {

    @Autowired
    private ProductCardRepository repository;

   /* @Override
    public List<ProductCard> findAll() {
        return repository.findAll();
    }*/

    @Override
    public Page<ProductCard> findAll(int pageNum) {
        return repository.findAll(PageRequest.of(pageNum,10));
    }

    @Override
    public ProductCard findOne(Integer id) {
        return repository.getOne(id);
    }

    @Override
    public ProductCard save(ProductCard entity) {
        return repository.save(entity);
    }

    @Override
    public void delete(Integer id) {
        repository.deleteById(id);
    }

    @Override
    public List<ProductCard> getByWarehouse_Id(Integer id) {
        return repository.getByWarehouse_Id(id);
    }
}
