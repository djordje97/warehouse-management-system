package ftn.poslovnainformatika.warehouse.service;

import ftn.poslovnainformatika.warehouse.entity.Product;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ProductServiceInterface {

    //List<Product> findAll();

    Page<Product> findAll(int pageNum);

    Product findOne(Integer id);

    Product save(Product product);

    void delete(Integer id);

    Page<Product> findByWarehouse(Integer id,int pageNum);
    Page<Product> search(String text,int pageNum);

}
