package ftn.poslovnainformatika.warehouse.service;

import ftn.poslovnainformatika.warehouse.entity.WarehouseCardAnalytic;
import org.springframework.data.domain.Page;

import java.util.List;

public interface WarehouseCardAnalyticServiceInterface {

    //List<WarehouseCardAnalytic> findAll();

    Page<WarehouseCardAnalytic> findAll(int pageNum);

    WarehouseCardAnalytic findOne(Integer id);

    WarehouseCardAnalytic save(WarehouseCardAnalytic warehouseCardAnalytic);

    void delete(Integer id);

    List<WarehouseCardAnalytic> findByProductCard(Integer id);
}
