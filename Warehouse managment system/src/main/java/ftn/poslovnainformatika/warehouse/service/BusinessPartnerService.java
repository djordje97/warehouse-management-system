package ftn.poslovnainformatika.warehouse.service;

import ftn.poslovnainformatika.warehouse.repository.BusinessPartnerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import  ftn.poslovnainformatika.warehouse.entity.BusinessPartner;
import java.util.List;

@Service
public class BusinessPartnerService implements  BusinessPartnerServiceInterface{

    @Autowired
    private BusinessPartnerRepository repository;

    /*@Override
    public List<BusinessPartner> findAll() {
        return null;
    }*/


    @Override
    public Page<BusinessPartner> findAll(int pageNum) {
        return repository.findAll(new PageRequest(pageNum,10));
    }

    @Override
    public BusinessPartner findOne(Integer id) {
        return repository.getOne(id);
    }

    @Override
    public BusinessPartner save(BusinessPartner entity) {
        return repository.save(entity);
    }

    @Override
    public void delete(Integer id) {
        repository.deleteById(id);
    }
}
