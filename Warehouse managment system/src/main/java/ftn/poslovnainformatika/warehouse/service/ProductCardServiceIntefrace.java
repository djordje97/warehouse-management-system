package ftn.poslovnainformatika.warehouse.service;

import ftn.poslovnainformatika.warehouse.entity.ProductCard;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ProductCardServiceIntefrace {

    //List<ProductCard> findAll();

    Page<ProductCard> findAll(int pageNum);

    ProductCard findOne(Integer id);

    ProductCard save(ProductCard entity);

    void delete(Integer id);

    List<ProductCard> getByWarehouse_Id(Integer id);

}
