package ftn.poslovnainformatika.warehouse.service;

import ftn.poslovnainformatika.warehouse.entity.MeasurementUnit;
import org.springframework.data.domain.Page;

import java.util.List;

public interface MeasurementUnitServiceInterface {

    List<MeasurementUnit> findAll();

    MeasurementUnit findOne(Integer id);

    MeasurementUnit save(MeasurementUnit measurementUnit);

    void delete(Integer id);
}
