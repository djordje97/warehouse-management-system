package ftn.poslovnainformatika.warehouse.service;

import ftn.poslovnainformatika.warehouse.entity.ProductCategory;
import ftn.poslovnainformatika.warehouse.repository.ProductCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductCategoryService implements ProductCategoryServiceInterface {

    @Autowired
    ProductCategoryRepository productCategoryRepository;


    /*@Override
    public List<ProductCategory> findAll() {
        return productCategoryRepository.findAll();
    }*/

    @Override
    public Page<ProductCategory> findAll(int pageNum) {
        return productCategoryRepository.findAll(PageRequest.of(pageNum,10));
    }

    @Override
    public ProductCategory findOne(Integer id) {
        return productCategoryRepository.getOne(id);
    }

    @Override
    public ProductCategory save(ProductCategory productCategory) {
        return productCategoryRepository.save(productCategory);
    }

    @Override
    public void delete(Integer id) {
        productCategoryRepository.deleteById(id);
    }
}
