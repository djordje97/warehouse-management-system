package ftn.poslovnainformatika.warehouse.service;

import ftn.poslovnainformatika.warehouse.entity.Company;
import ftn.poslovnainformatika.warehouse.repository.CompanyRepositoory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService implements CompanyServiceInterface {

    @Autowired
    private CompanyRepositoory companyRepository;

    /*@Override
    public List<Company> findAll() {
        return companyRepository.findAll();
    }*/

    @Override
    public Page<Company> findAll(int pageNum) {
        return companyRepository.findAll(new PageRequest(pageNum,10));
    }

    @Override
    public Company findOne(Integer id) {
        return companyRepository.getOne(id);
    }

    @Override
    public Company save(Company company) {
        return companyRepository.save(company);
    }

    @Override
    public void delete(Integer id) {
        companyRepository.deleteById(id);
    }
}
