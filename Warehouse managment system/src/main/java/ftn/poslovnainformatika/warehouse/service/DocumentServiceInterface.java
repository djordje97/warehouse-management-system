package ftn.poslovnainformatika.warehouse.service;

import ftn.poslovnainformatika.warehouse.entity.Document;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface DocumentServiceInterface {

    //List<Document> findAll();

    Page<Document> findAll(int pageNum);

    Document findOne(Integer id);

    Document save(Document document);

    void delete(Integer id);

}
