package ftn.poslovnainformatika.warehouse.dto.support;

import ftn.poslovnainformatika.warehouse.dto.ProductCardDTO;
import ftn.poslovnainformatika.warehouse.entity.BusinessYear;
import ftn.poslovnainformatika.warehouse.entity.ProductCard;
import ftn.poslovnainformatika.warehouse.entity.Warehouse;
import ftn.poslovnainformatika.warehouse.service.BusinessYearServiceInterface;
import ftn.poslovnainformatika.warehouse.service.WarehouseServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ProductCardDTOToProductCard implements Converter<ProductCardDTO,ProductCard> {

    @Autowired
    BusinessYearServiceInterface businessYearService;

    @Autowired
    WarehouseServiceInterface warehouseService;

    @Override
    public ProductCard convert(ProductCardDTO productCardDTO) {
        ProductCard pc = new ProductCard();
        pc.setId(productCardDTO.getId());
        pc.setInitialStateQuantity(productCardDTO.getInitialStateQuantity());
        pc.setTrafficEntryQuantity(productCardDTO.getTrafficEntryQuantity());
        pc.setTrafficEntryValue(productCardDTO.getTrafficEntryValue());
        pc.setTrafficExitQuantity(productCardDTO.getTrafficExitQuantity());
        pc.setTrafficExitValue(productCardDTO.getTrafficExitValue());
        pc.setInitialStateValue(productCardDTO.getInitialStateValue());
        pc.setTotalQuantity();
        pc.setTotalValue();
        pc.setPrice(productCardDTO.getPrice());
        pc.setPurchasePrice(productCardDTO.getPurchasePrice());

        BusinessYear businessYear = businessYearService.findOne(productCardDTO.getBusinessYear().getId());
        if(businessYear != null){
            pc.setBusinessYear(businessYear);
        }

        Warehouse warehouse = warehouseService.findOne(productCardDTO.getWarehouse().getId());
        if(warehouse != null){
            pc.setWarehouse(warehouse);
        }

        return pc;
    }
}
