package ftn.poslovnainformatika.warehouse.dto;

import ftn.poslovnainformatika.warehouse.entity.Company;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class CompanyDTO {

    private Integer id;
    @NotBlank(message = "Name of company cannot be empty")
    private String name;
    @NotBlank(message = "PIB cannot be empty")
    private String PIB;
    private PlaceDTO place;
    private EmployeeDTO employee;

    public CompanyDTO() {
        super();
    }

    public CompanyDTO(Integer id, String name, String PIB, PlaceDTO place, EmployeeDTO employee) {
        this.id = id;
        this.name = name;
        this.PIB = PIB;
        this.place = place;
        this.employee = employee;
    }

    public CompanyDTO(Company company){
        this(company.getId(),
                company.getName(),
                company.getPIB(),
                new PlaceDTO(company.getPlace()),
                new EmployeeDTO(company.getEmployee()));
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPIB() {
        return PIB;
    }

    public void setPIB(String PIB) {
        this.PIB = PIB;
    }

    public PlaceDTO getPlace() {
        return place;
    }

    public void setPlace(PlaceDTO place) {
        this.place = place;
    }

    public EmployeeDTO getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeDTO employee) {
        this.employee = employee;
    }
}
