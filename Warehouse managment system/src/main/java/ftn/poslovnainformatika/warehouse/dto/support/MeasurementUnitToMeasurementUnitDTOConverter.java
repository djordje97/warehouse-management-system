package ftn.poslovnainformatika.warehouse.dto.support;


import ftn.poslovnainformatika.warehouse.dto.MeasurementUnitDTO;
import ftn.poslovnainformatika.warehouse.entity.MeasurementUnit;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MeasurementUnitToMeasurementUnitDTOConverter implements Converter<MeasurementUnit,MeasurementUnitDTO>{
    @Override
    public MeasurementUnitDTO convert(MeasurementUnit measurementUnit) {
        MeasurementUnitDTO dto = new MeasurementUnitDTO();
        dto.setId(measurementUnit.getId());
        dto.setName(measurementUnit.getName());

        return dto;
    }

    public List<MeasurementUnitDTO> convert(List<MeasurementUnit> measurementUnits){
        List<MeasurementUnitDTO> retVal = new ArrayList<>();
        for(MeasurementUnit mu: measurementUnits){
            retVal.add(convert(mu));
        }

        return retVal;
    }
}
