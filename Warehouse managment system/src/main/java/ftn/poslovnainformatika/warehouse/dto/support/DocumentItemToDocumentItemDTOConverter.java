package ftn.poslovnainformatika.warehouse.dto.support;

import ftn.poslovnainformatika.warehouse.dto.DocumentDTO;
import ftn.poslovnainformatika.warehouse.dto.DocumentItemDTO;
import ftn.poslovnainformatika.warehouse.dto.ProductDTO;
import ftn.poslovnainformatika.warehouse.entity.DocumentItem;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DocumentItemToDocumentItemDTOConverter implements Converter<DocumentItem,DocumentItemDTO> {
    @Override
    public DocumentItemDTO convert(DocumentItem documentItem) {
        DocumentItemDTO dto = new DocumentItemDTO();
        dto.setId(documentItem.getId());
        dto.setPrice(documentItem.getPrice());
        dto.setQuantity(documentItem.getQuantity());
        dto.setValue(documentItem.getValue());
        dto.setDocument(new DocumentDTO(documentItem.getDocument()));
        dto.setProduct(new ProductDTO(documentItem.getProduct()));

        return dto;
    }

    public List<DocumentItemDTO> convert(List<DocumentItem> documentItems){
        List<DocumentItemDTO> retVal = new ArrayList<>();
        for(DocumentItem di:documentItems){
            retVal.add(convert(di));
        }

        return retVal;
    }
}
