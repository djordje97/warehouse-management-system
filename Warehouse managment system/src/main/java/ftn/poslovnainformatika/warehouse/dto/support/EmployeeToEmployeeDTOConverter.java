package ftn.poslovnainformatika.warehouse.dto.support;

import ftn.poslovnainformatika.warehouse.dto.EmployeeDTO;
import ftn.poslovnainformatika.warehouse.dto.PlaceDTO;
import ftn.poslovnainformatika.warehouse.entity.Employee;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class EmployeeToEmployeeDTOConverter implements Converter<Employee,EmployeeDTO> {
    @Override
    public EmployeeDTO convert(Employee employee) {
        EmployeeDTO dto = new EmployeeDTO();
        dto.setId(employee.getId());
        dto.setFirstName(employee.getFirstName());
        dto.setLastName(employee.getLastName());
        dto.setJMBG(employee.getJMBG());
        dto.setAddress(employee.getAddress());
        dto.setPlace(new PlaceDTO(employee.getPlace()));

        return dto;
    }

    public List<EmployeeDTO> convert(List<Employee> employees){
        List<EmployeeDTO> retVal = new ArrayList<>();
        for(Employee e:employees){
            retVal.add(convert(e));
        }

        return retVal;
    }
}
