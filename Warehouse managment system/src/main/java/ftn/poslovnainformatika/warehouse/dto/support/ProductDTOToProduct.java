package ftn.poslovnainformatika.warehouse.dto.support;

import ftn.poslovnainformatika.warehouse.dto.ProductDTO;
import ftn.poslovnainformatika.warehouse.entity.*;
import ftn.poslovnainformatika.warehouse.service.DocumentItemServiceInterface;
import ftn.poslovnainformatika.warehouse.service.MeasurementUnitServiceInterface;
import ftn.poslovnainformatika.warehouse.service.ProductCardServiceIntefrace;
import ftn.poslovnainformatika.warehouse.service.ProductCategoryServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ProductDTOToProduct implements Converter<ProductDTO,Product> {

    @Autowired
    ProductCardServiceIntefrace productCardService;

    @Autowired
    MeasurementUnitServiceInterface measurementUnitService;

    @Autowired
    DocumentItemServiceInterface documentItemService;

    @Autowired
    ProductCategoryServiceInterface productCategoryService;

    @Override
    public Product convert(ProductDTO productDTO) {
        Product p = new Product();
        p.setId(productDTO.getId());
        p.setProductName(productDTO.getProductName());

        ProductCard productCard = productCardService.findOne(productDTO.getProductCard().getId());
        if(productCard != null){
            p.setProductCard(productCard);
        }

        MeasurementUnit measurementUnit = measurementUnitService.findOne(productDTO.getMeasurementUnit().getId());
        if(measurementUnit != null){
            p.setMeasurementUnit(measurementUnit);
        }

        ProductCategory productCategory = productCategoryService.findOne(productDTO.getProductCategory().getId());
        if(productCategory != null){
            p.setProductCategory(productCategory);
        }

        return p;
    }
}
