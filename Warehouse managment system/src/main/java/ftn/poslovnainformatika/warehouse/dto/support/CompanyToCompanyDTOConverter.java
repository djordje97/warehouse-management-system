package ftn.poslovnainformatika.warehouse.dto.support;

import ftn.poslovnainformatika.warehouse.dto.CompanyDTO;
import ftn.poslovnainformatika.warehouse.dto.EmployeeDTO;
import ftn.poslovnainformatika.warehouse.dto.PlaceDTO;
import ftn.poslovnainformatika.warehouse.entity.Company;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CompanyToCompanyDTOConverter implements Converter<Company,CompanyDTO> {
    @Override
    public CompanyDTO convert(Company company) {
        CompanyDTO dto = new CompanyDTO();
        dto.setId(company.getId());
        dto.setName(company.getName());
        dto.setPIB(company.getPIB());
        dto.setEmployee(new EmployeeDTO(company.getEmployee()));
        dto.setPlace(new PlaceDTO(company.getPlace()));

        return dto;
    }

    public List<CompanyDTO> convert(List<Company> companies){
        List<CompanyDTO> retVal = new ArrayList<>();
        for(Company c: companies){
            retVal.add(convert(c));
        }

        return retVal;
    }
}
