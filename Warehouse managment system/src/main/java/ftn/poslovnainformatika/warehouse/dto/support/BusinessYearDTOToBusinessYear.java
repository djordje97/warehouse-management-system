package ftn.poslovnainformatika.warehouse.dto.support;

import ftn.poslovnainformatika.warehouse.dto.BusinessYearDTO;
import ftn.poslovnainformatika.warehouse.entity.BusinessYear;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class BusinessYearDTOToBusinessYear implements Converter<BusinessYearDTO,BusinessYear> {
    @Override
    public BusinessYear convert(BusinessYearDTO businessYearDTO) {
        BusinessYear by = new BusinessYear();
        by.setId(businessYearDTO.getId());
        by.setClosed(businessYearDTO.isClosed());
        by.setYear(businessYearDTO.getYear());
        return by;
    }
}
