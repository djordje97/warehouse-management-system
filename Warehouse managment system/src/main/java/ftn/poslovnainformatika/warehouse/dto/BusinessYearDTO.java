package ftn.poslovnainformatika.warehouse.dto;

import ftn.poslovnainformatika.warehouse.entity.BusinessYear;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class BusinessYearDTO {

    private Integer id;
    @NotNull(message = "Year cannot be empty")
    private int year;
    private boolean isClosed;

    public BusinessYearDTO() {
        super();
    }

    public BusinessYearDTO(Integer id, int year, boolean isClosed) {
        this.id = id;
        this.year = year;
        this.isClosed = isClosed;
    }

    public BusinessYearDTO(BusinessYear businessYear){
        this(businessYear.getId(),
                businessYear.getYear(),
                businessYear.isClosed());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public boolean isClosed() {
        return isClosed;
    }

    public void setClosed(boolean closed) {
        isClosed = closed;
    }
}
