package ftn.poslovnainformatika.warehouse.dto.support;

import ftn.poslovnainformatika.warehouse.dto.BusinessPartnerDTO;
import ftn.poslovnainformatika.warehouse.dto.PlaceDTO;
import ftn.poslovnainformatika.warehouse.entity.BusinessPartner;
import ftn.poslovnainformatika.warehouse.service.PlaceService;
import ftn.poslovnainformatika.warehouse.service.PlaceServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BusinessPartnerToBusinessPartnerDTOConverter implements Converter<BusinessPartner,BusinessPartnerDTO> {
    @Autowired
    PlaceServiceInterface placeService;

    @Override
    public BusinessPartnerDTO convert(BusinessPartner businessPartner) {
        BusinessPartnerDTO dto = new BusinessPartnerDTO();
        dto.setId(businessPartner.getId());
        dto.setName(businessPartner.getName());
        dto.setAddress(businessPartner.getAddress());
        dto.setPartnerType(businessPartner.getPartnerType());
        dto.setPIB(businessPartner.getPib());
        dto.setPlace(new PlaceDTO(businessPartner.getPlace()));

        return dto;
    }

    public List<BusinessPartnerDTO> convert(List<BusinessPartner> businessPartners){
        List<BusinessPartnerDTO> retVal = new ArrayList<>();
        for(BusinessPartner bp: businessPartners){
            retVal.add(convert(bp));
        }

        return retVal;
    }

    /*public BusinessPartner convert(BusinessPartnerDTO businessPartnerDTO) {
        BusinessPartner bp = new BusinessPartner();
        bp.setId(businessPartnerDTO.getId());
        bp.setName(businessPartnerDTO.getName());
        bp.setAddress(businessPartnerDTO.getAddress());
        bp.setPartnerType(businessPartnerDTO.getPartnerType());
        bp.setPib(businessPartnerDTO.getPIB());
        bp.setPlace(placeService.findByName(businessPartnerDTO.getName()));

        return bp;
    }*/
}
