package ftn.poslovnainformatika.warehouse.dto.support;

import ftn.poslovnainformatika.warehouse.dto.DocumentItemDTO;
import ftn.poslovnainformatika.warehouse.entity.Document;
import ftn.poslovnainformatika.warehouse.entity.DocumentItem;
import ftn.poslovnainformatika.warehouse.entity.Product;
import ftn.poslovnainformatika.warehouse.service.DocumentServiceInterface;
import ftn.poslovnainformatika.warehouse.service.ProductServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class DocumentItemDTOToDocumentItem implements Converter<DocumentItemDTO,DocumentItem> {

    @Autowired
    private DocumentServiceInterface documentService;

    @Autowired
    private ProductServiceInterface productService;

    @Override
    public DocumentItem convert(DocumentItemDTO documentItemDTO) {
        DocumentItem di = new DocumentItem();
        di.setId(documentItemDTO.getId());
        di.setPrice(documentItemDTO.getPrice());
        di.setQuantity(documentItemDTO.getQuantity());
        di.setValue(documentItemDTO.getValue());


        Document document = documentService.findOne(documentItemDTO.getDocument().getId());
        if(document != null){
            di.setDocument(document);
        }

        Product product = productService.findOne(documentItemDTO.getProduct().getId());
        if(product != null){
            di.setProduct(product);
        }

        return di;
    }
}
