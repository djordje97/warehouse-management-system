package ftn.poslovnainformatika.warehouse.dto.support;

import ftn.poslovnainformatika.warehouse.dto.BusinessYearDTO;
import ftn.poslovnainformatika.warehouse.dto.ProductCardDTO;
import ftn.poslovnainformatika.warehouse.dto.WarehouseDTO;
import ftn.poslovnainformatika.warehouse.entity.ProductCard;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProductCardToProductCardDTOConverter implements Converter<ProductCard,ProductCardDTO> {
    @Override
    public ProductCardDTO convert(ProductCard productCard) {
        ProductCardDTO dto = new ProductCardDTO();
        dto.setId(productCard.getId());
        dto.setInitialStateQuantity(productCard.getInitialStateQuantity());
        dto.setInitialStateValue(productCard.getInitialStateValue());
        dto.setTotalQuantity(productCard.getTotalQuantity());
        dto.setTotalValue(productCard.getTotalValue());
        dto.setTrafficEntryQuantity(productCard.getTrafficEntryQuantity());
        dto.setTrafficEntryValue(productCard.getTrafficEntryValue());
        dto.setTrafficExitQuantity(productCard.getTrafficExitQuantity());
        dto.setTrafficExitValue(productCard.getTrafficExitValue());
        dto.setBusinessYear(new BusinessYearDTO(productCard.getBusinessYear()));
        dto.setWarehouse(new WarehouseDTO(productCard.getWarehouse()));
        dto.setPrice(productCard.getPrice());
        dto.setPurchasePrice(productCard.getPurchasePrice());

        return dto;
    }

    public List<ProductCardDTO> convert(List<ProductCard> productCards){
        List<ProductCardDTO> retVal = new ArrayList<>();
        for(ProductCard pc:productCards){
            retVal.add(convert(pc));
        }

        return retVal;
    }
}
