package ftn.poslovnainformatika.warehouse.dto.support;

import ftn.poslovnainformatika.warehouse.dto.WarehouseCardAnalyticDTO;
import ftn.poslovnainformatika.warehouse.entity.DocumentItem;
import ftn.poslovnainformatika.warehouse.entity.ProductCard;
import ftn.poslovnainformatika.warehouse.entity.WarehouseCardAnalytic;
import ftn.poslovnainformatika.warehouse.service.DocumentItemServiceInterface;
import ftn.poslovnainformatika.warehouse.service.ProductCardServiceIntefrace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class WarehouseCardAnalyticDTOToWarehouseCardAnalytic implements Converter<WarehouseCardAnalyticDTO,WarehouseCardAnalytic> {

    @Autowired
    ProductCardServiceIntefrace productCardService;

    @Autowired
    DocumentItemServiceInterface documentItemService;

    @Override
    public WarehouseCardAnalytic convert(WarehouseCardAnalyticDTO warehouseCardAnalyticDTO) {
        WarehouseCardAnalytic wca = new WarehouseCardAnalytic();
        wca.setId(warehouseCardAnalyticDTO.getId());
        wca.setDirection(warehouseCardAnalyticDTO.getDirection());
        wca.setTrafficType(warehouseCardAnalyticDTO.getTrafficType());
        wca.setOrdinalNumber(warehouseCardAnalyticDTO.getOrdinalNumber());
        wca.setPrice(warehouseCardAnalyticDTO.getPrice());
        wca.setQuantity(warehouseCardAnalyticDTO.getQuantity());
        wca.setTotalValue(warehouseCardAnalyticDTO.getTotalValue());

        ProductCard productCard = productCardService.findOne(warehouseCardAnalyticDTO.getProductCard().getId());
        if(productCard != null){
            wca.setProductCard(productCard);
        }

        DocumentItem documentItem = documentItemService.findOne(warehouseCardAnalyticDTO.getDocumentItem().getId());
        if(documentItem != null){
            wca.setDocumentItem(documentItem);
        }

        return null;
    }
}
