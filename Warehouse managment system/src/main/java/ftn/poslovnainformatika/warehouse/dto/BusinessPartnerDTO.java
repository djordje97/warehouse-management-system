package ftn.poslovnainformatika.warehouse.dto;

import ftn.poslovnainformatika.warehouse.entity.BusinessPartner;

import javax.validation.constraints.NotBlank;

public class BusinessPartnerDTO {

    private Integer id;
    @NotBlank(message = "Name of business partner cannot be empty")
    private String name;
    @NotBlank(message = "PIB cannot be empty")
    private String PIB;
    @NotBlank(message = "Address cannot be empty")
    private String address;
    @NotBlank(message = "Partner type cannot be empty")
    private String partnerType;
    private PlaceDTO place;

    public BusinessPartnerDTO() {
        super();
    }

    public BusinessPartnerDTO(Integer id, String name, String PIB, String address, String partnerType, PlaceDTO place) {
        this.id = id;
        this.name = name;
        this.PIB = PIB;
        this.address = address;
        this.partnerType = partnerType;
        this.place = place;
    }

    public BusinessPartnerDTO(BusinessPartner businessPartner){
        this(businessPartner.getId(),
                businessPartner.getName(),
                businessPartner.getPib(),
                businessPartner.getAddress(),
                businessPartner.getPartnerType(),
                new PlaceDTO(businessPartner.getPlace()));
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPIB() {
        return PIB;
    }

    public void setPIB(String PIB) {
        this.PIB = PIB;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(String partnerType) {
        this.partnerType = partnerType;
    }

    public PlaceDTO getPlace() {
        return place;
    }

    public void setPlace(PlaceDTO place) {
        this.place = place;
    }
}
