package ftn.poslovnainformatika.warehouse.dto.support;

import ftn.poslovnainformatika.warehouse.dto.ProductCategoryDTO;
import ftn.poslovnainformatika.warehouse.entity.ProductCategory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ProductCategoryDTOToProductCategory implements Converter<ProductCategoryDTO,ProductCategory> {
    @Override
    public ProductCategory convert(ProductCategoryDTO productCategoryDTO) {
        ProductCategory pc = new ProductCategory();
        pc.setId(productCategoryDTO.getId());
        pc.setProductCategoryName(productCategoryDTO.getProductCategoryName());

        return pc;
    }
}
