package ftn.poslovnainformatika.warehouse.dto;

import ftn.poslovnainformatika.warehouse.entity.WarehouseCardAnalytic;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class WarehouseCardAnalyticDTO {

    private Integer id;
    @NotNull(message = "Ordinal number cannot be empty")
    private Integer ordinalNumber;
    @NotBlank(message = "Traffic type cannot be empty")
    private String trafficType;
    @NotBlank(message = "Direction cannot be empty")
    private String direction;
    @NotNull(message = "Quantity cannot be empty")
    private double quantity;
    @NotNull(message = "Price cannot be empty")
    private double price;
    @NotNull(message = "Total value cannot be empty")
    private double totalValue;
    private ProductCardDTO productCard;
    private  DocumentItemDTO documentItem;

    public WarehouseCardAnalyticDTO() {
        super();
    }

    public WarehouseCardAnalyticDTO(Integer id, Integer ordinalNumber, String trafficType, String direction, double quantity, double price, double totalValue, ProductCardDTO productCard, DocumentItemDTO documentItem) {
        this.id = id;
        this.ordinalNumber = ordinalNumber;
        this.trafficType = trafficType;
        this.direction = direction;
        this.quantity = quantity;
        this.price = price;
        this.totalValue = totalValue;
        this.productCard = productCard;
        this.documentItem = documentItem;
    }

    public WarehouseCardAnalyticDTO(WarehouseCardAnalytic warehouseCardAnalytic){
        this(warehouseCardAnalytic.getId(),
                warehouseCardAnalytic.getOrdinalNumber(),
                warehouseCardAnalytic.getTrafficType(),
                warehouseCardAnalytic.getDirection(),
                warehouseCardAnalytic.getQuantity(),
                warehouseCardAnalytic.getPrice(),
                warehouseCardAnalytic.getTotalValue(),
                new ProductCardDTO(warehouseCardAnalytic.getProductCard()),
                new DocumentItemDTO(warehouseCardAnalytic.getDocumentItem()));
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrdinalNumber() {
        return ordinalNumber;
    }

    public void setOrdinalNumber(Integer ordinalNumber) {
        this.ordinalNumber = ordinalNumber;
    }

    public String getTrafficType() {
        return trafficType;
    }

    public void setTrafficType(String trafficType) {
        this.trafficType = trafficType;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(double totalValue) {
        this.totalValue = totalValue;
    }

    public ProductCardDTO getProductCard() {
        return productCard;
    }

    public void setProductCard(ProductCardDTO productCard) {
        this.productCard = productCard;
    }

    public DocumentItemDTO getDocumentItem() {
        return documentItem;
    }

    public void setDocumentItem(DocumentItemDTO documentItem) {
        this.documentItem = documentItem;
    }
}
