package ftn.poslovnainformatika.warehouse.dto.support;

import ftn.poslovnainformatika.warehouse.dto.CompanyDTO;
import ftn.poslovnainformatika.warehouse.entity.Company;
import ftn.poslovnainformatika.warehouse.entity.Employee;
import ftn.poslovnainformatika.warehouse.entity.Place;
import ftn.poslovnainformatika.warehouse.service.EmployeeServiceInterface;
import ftn.poslovnainformatika.warehouse.service.PlaceServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CompanyDTOToCompany implements Converter<CompanyDTO,Company> {

    @Autowired
    private PlaceServiceInterface placeService;

    @Autowired
    private EmployeeServiceInterface employeeService;

    @Override
    public Company convert(CompanyDTO companyDTO) {
        Company c = new Company();
        c.setId(companyDTO.getId());
        c.setName(companyDTO.getName());
        c.setPIB(companyDTO.getPIB());

        Place place = placeService.findByName(companyDTO.getPlace().getName());
        if(place != null){
            c.setPlace(place);
        }

        Employee employee = employeeService.findOne(companyDTO.getEmployee().getId());
        if(employee != null){
            c.setEmployee(employee);
        }

        return c;
    }
}
