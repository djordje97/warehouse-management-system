package ftn.poslovnainformatika.warehouse.dto;

import ftn.poslovnainformatika.warehouse.entity.Place;

import javax.validation.constraints.NotBlank;

public class PlaceDTO {

    private Integer id;
    @NotBlank(message = "Name of place cannot be empty")
    private String name;
    private Integer zipCode;

    public PlaceDTO() {
        super();
    }

    public PlaceDTO(Integer id, String name, Integer zipCode) {
        this.id = id;
        this.name = name;
        this.zipCode = zipCode;
    }

    public PlaceDTO(Place place){
        this(place.getId(),
                place.getName(),
                place.getZipCode());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getZipCode() {
        return zipCode;
    }

    public void setZipCode(Integer zipCode) {
        this.zipCode = zipCode;
    }
}
