package ftn.poslovnainformatika.warehouse.dto.support;

import ftn.poslovnainformatika.warehouse.dto.ProductCardDTO;
import ftn.poslovnainformatika.warehouse.dto.WarehouseCardAnalyticDTO;
import ftn.poslovnainformatika.warehouse.entity.WarehouseCardAnalytic;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class WarehouseCardAnalyticToWarehouseCardAnalyticDTOConverter implements Converter<WarehouseCardAnalytic,WarehouseCardAnalyticDTO> {
    @Override
    public WarehouseCardAnalyticDTO convert(WarehouseCardAnalytic warehouseCardAnalytic) {
        WarehouseCardAnalyticDTO dto = new WarehouseCardAnalyticDTO();
        dto.setId(warehouseCardAnalytic.getId());
        dto.setDirection(warehouseCardAnalytic.getDirection());
        dto.setOrdinalNumber(warehouseCardAnalytic.getOrdinalNumber());
        dto.setPrice(warehouseCardAnalytic.getPrice());
        dto.setQuantity(warehouseCardAnalytic.getQuantity());
        dto.setTotalValue(warehouseCardAnalytic.getTotalValue());
        dto.setTrafficType(warehouseCardAnalytic.getTrafficType());
        dto.setProductCard(new ProductCardDTO(warehouseCardAnalytic.getProductCard()));

        return dto;
    }

    public List<WarehouseCardAnalyticDTO> convert(List<WarehouseCardAnalytic> warehouseCardAnalytics){
        List<WarehouseCardAnalyticDTO> retVal = new ArrayList<>();
        for(WarehouseCardAnalytic wca: warehouseCardAnalytics){
            retVal.add(convert(wca));
        }

        return retVal;
    }
}
