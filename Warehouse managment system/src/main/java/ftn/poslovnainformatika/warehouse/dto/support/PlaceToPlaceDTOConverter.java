package ftn.poslovnainformatika.warehouse.dto.support;

import ftn.poslovnainformatika.warehouse.dto.PlaceDTO;
import ftn.poslovnainformatika.warehouse.entity.Place;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PlaceToPlaceDTOConverter implements Converter<Place,PlaceDTO>{
    @Override
    public PlaceDTO convert(Place place) {
        PlaceDTO dto = new PlaceDTO();
        dto.setId(place.getId());
        dto.setName(place.getName());
        dto.setZipCode(place.getZipCode());

        return dto;
    }

    public List<PlaceDTO> convert(List<Place> places){
        List<PlaceDTO> retVal = new ArrayList<>();
        for(Place p: places){
            retVal.add(convert(p));
        }

        return retVal;
    }
}
