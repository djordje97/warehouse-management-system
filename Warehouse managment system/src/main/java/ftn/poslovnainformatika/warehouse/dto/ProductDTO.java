package ftn.poslovnainformatika.warehouse.dto;

import ftn.poslovnainformatika.warehouse.entity.Product;

import javax.validation.constraints.NotBlank;

public class ProductDTO {

    private Integer id;
    @NotBlank(message = "Product name cannot be null")
    private String productName;
    private MeasurementUnitDTO measurementUnit;
    private ProductCardDTO productCard;
    private ProductCategoryDTO productCategory;

    public ProductDTO() {
        super();
    }

    public ProductDTO(Integer id, String productName, MeasurementUnitDTO measurementUnit, ProductCardDTO productCard, ProductCategoryDTO productCategory) {
        this.id = id;
        this.productName = productName;
        this.measurementUnit = measurementUnit;

        this.productCard = productCard;
        this.productCategory = productCategory;
    }

    public ProductDTO(Product product){
        this(product.getId(),
                product.getProductName(),
                new MeasurementUnitDTO(product.getMeasurementUnit()),
                new ProductCardDTO(product.getProductCard()),
                new ProductCategoryDTO(product.getProductCategory()));
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public MeasurementUnitDTO getMeasurementUnit() {
        return measurementUnit;
    }

    public void setMeasurementUnit(MeasurementUnitDTO measurementUnit) {
        this.measurementUnit = measurementUnit;
    }

    public ProductCardDTO getProductCard() {
        return productCard;
    }

    public void setProductCard(ProductCardDTO productCard) {
        this.productCard = productCard;
    }

    public ProductCategoryDTO getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(ProductCategoryDTO productCategory) {
        this.productCategory = productCategory;
    }
}
