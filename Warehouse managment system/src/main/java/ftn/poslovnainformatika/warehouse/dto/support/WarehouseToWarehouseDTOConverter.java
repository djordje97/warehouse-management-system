package ftn.poslovnainformatika.warehouse.dto.support;

import ftn.poslovnainformatika.warehouse.dto.CompanyDTO;
import ftn.poslovnainformatika.warehouse.dto.EmployeeDTO;
import ftn.poslovnainformatika.warehouse.dto.WarehouseDTO;
import ftn.poslovnainformatika.warehouse.entity.Warehouse;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class WarehouseToWarehouseDTOConverter implements Converter<Warehouse,WarehouseDTO> {
    @Override
    public WarehouseDTO convert(Warehouse warehouse) {
        WarehouseDTO dto = new WarehouseDTO();
        dto.setId(warehouse.getId());
        dto.setName(warehouse.getName());
        dto.setCompany(new CompanyDTO(warehouse.getCompany()));
        dto.setEmployee(new EmployeeDTO(warehouse.getEmployee()));

        return dto;
    }

    public List<WarehouseDTO> convert(List<Warehouse> warehouses){
        List<WarehouseDTO> retVal = new ArrayList<>();
        for(Warehouse w: warehouses){
            retVal.add(convert(w));
        }

        return retVal;
    }
}
