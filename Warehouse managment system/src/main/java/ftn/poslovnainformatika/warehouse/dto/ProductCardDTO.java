package ftn.poslovnainformatika.warehouse.dto;

import ftn.poslovnainformatika.warehouse.entity.ProductCard;

import javax.validation.constraints.NotNull;

public class ProductCardDTO {

    private Integer id;
    @NotNull(message = "Initial state of quantity cannot be empty")
    private double initialStateQuantity;
    @NotNull(message = "Initial state of value cannot be empty")
    private double initialStateValue;
    @NotNull(message = "Traffic entry quantity cannot be empty")
    private double trafficEntryQuantity;
    @NotNull(message = "Traffic entry value cannot be empty")
    private double trafficEntryValue;
    @NotNull(message = "Traffic exit quantity cannot be empty")
    private double trafficExitQuantity;
    @NotNull(message = "Traffic exit value cannot be empty")
    private double trafficExitValue;
    @NotNull(message = "Total quantity cannot be null")
    private double totalQuantity;
    @NotNull(message = "Total value cannot be empty")
    private double totalValue;
    private BusinessYearDTO businessYear;
    private WarehouseDTO warehouse;
    @NotNull(message = "Price cannot be empty")
    private  double price;
    @NotNull(message = "Purchase price cannot be empty")
    private  double purchasePrice;

    public ProductCardDTO() {
        super();
    }

    public ProductCardDTO(Integer id, double initialStateQuantity, double initialStateValue, double trafficEntryQuantity, double trafficEntryValue, double trafficExitQuantity, double trafficExitValue, double totalQuantity, double totalValue, BusinessYearDTO businessYear, WarehouseDTO warehouse, double price, double purchasePrice) {
        this.id = id;
        this.initialStateQuantity = initialStateQuantity;
        this.initialStateValue = initialStateValue;
        this.trafficEntryQuantity = trafficEntryQuantity;
        this.trafficEntryValue = trafficEntryValue;
        this.trafficExitQuantity = trafficExitQuantity;
        this.trafficExitValue = trafficExitValue;
        this.totalQuantity = totalQuantity;
        this.totalValue = totalValue;
        this.businessYear = businessYear;
        this.warehouse = warehouse;
        this.price = price;
        this.purchasePrice = purchasePrice;
    }

    public ProductCardDTO(ProductCard productCard){
        this(productCard.getId(),
                productCard.getInitialStateQuantity(),
                productCard.getInitialStateValue(),
                productCard.getTrafficEntryQuantity(),
                productCard.getTrafficEntryValue(),
                productCard.getTrafficExitQuantity(),
                productCard.getTrafficExitValue(),
                productCard.getTotalQuantity(),
                productCard.getTotalValue(),
                new BusinessYearDTO(productCard.getBusinessYear()),
                new WarehouseDTO(productCard.getWarehouse()),
                productCard.getPrice(),
                productCard.getPurchasePrice());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getInitialStateQuantity() {
        return initialStateQuantity;
    }

    public void setInitialStateQuantity(double initialStateQuantity) {
        this.initialStateQuantity = initialStateQuantity;
    }

    public double getInitialStateValue() {
        return initialStateValue;
    }

    public void setInitialStateValue(double initialStateValue) {
        this.initialStateValue = initialStateValue;
    }

    public double getTrafficEntryQuantity() {
        return trafficEntryQuantity;
    }

    public void setTrafficEntryQuantity(double trafficEntryQuantity) {
        this.trafficEntryQuantity = trafficEntryQuantity;
    }

    public double getTrafficEntryValue() {
        return trafficEntryValue;
    }

    public void setTrafficEntryValue(double trafficEntryValue) {
        this.trafficEntryValue = trafficEntryValue;
    }

    public double getTrafficExitQuantity() {
        return trafficExitQuantity;
    }

    public void setTrafficExitQuantity(double trafficExitQuantity) {
        this.trafficExitQuantity = trafficExitQuantity;
    }

    public double getTrafficExitValue() {
        return trafficExitValue;
    }

    public void setTrafficExitValue(double trafficExitValue) {
        this.trafficExitValue = trafficExitValue;
    }

    public double getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(double totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public double getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(double totalValue) {
        this.totalValue = totalValue;
    }

    public BusinessYearDTO getBusinessYear() {
        return businessYear;
    }

    public void setBusinessYear(BusinessYearDTO businessYear) {
        this.businessYear = businessYear;
    }

    public WarehouseDTO getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(WarehouseDTO warehouse) {
        this.warehouse = warehouse;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(double purchasePrice) {
        this.purchasePrice = purchasePrice;
    }
}
