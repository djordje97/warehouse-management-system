package ftn.poslovnainformatika.warehouse.dto.support;

import ftn.poslovnainformatika.warehouse.dto.*;
import ftn.poslovnainformatika.warehouse.entity.Product;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProductToProductDTOConverter implements Converter<Product, ProductDTO> {
    @Override
    public ProductDTO convert(Product product) {
        ProductDTO dto = new ProductDTO();
        dto.setId(product.getId());
        dto.setProductName(product.getProductName());
        dto.setMeasurementUnit(new MeasurementUnitDTO(product.getMeasurementUnit()));
        dto.setProductCard(new ProductCardDTO(product.getProductCard()));
        dto.setProductCategory(new ProductCategoryDTO(product.getProductCategory()));

        return dto;
    }

    public List<ProductDTO> convert(List<Product> products){
        List<ProductDTO> retVal = new ArrayList<>();
        for(Product p: products){
            retVal.add(convert(p));
        }

        return retVal;
    }
}
