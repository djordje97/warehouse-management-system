package ftn.poslovnainformatika.warehouse.dto;

import ftn.poslovnainformatika.warehouse.entity.ProductCategory;

import javax.validation.constraints.NotBlank;

public class ProductCategoryDTO {

    private Integer id;
    @NotBlank(message = "Product category name cannot be empty")
    private String productCategoryName;

    public ProductCategoryDTO() {
        super();
    }

    public ProductCategoryDTO(Integer id, String productCategoryName) {
        this.id = id;
        this.productCategoryName = productCategoryName;
    }

    public ProductCategoryDTO(ProductCategory productCategory){
        this(productCategory.getId(),
                productCategory.getProductCategoryName());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProductCategoryName() {
        return productCategoryName;
    }

    public void setProductCategoryName(String productCategoryName) {
        this.productCategoryName = productCategoryName;
    }
}
