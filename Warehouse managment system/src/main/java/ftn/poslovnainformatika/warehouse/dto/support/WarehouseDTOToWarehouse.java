package ftn.poslovnainformatika.warehouse.dto.support;

import ftn.poslovnainformatika.warehouse.dto.WarehouseDTO;
import ftn.poslovnainformatika.warehouse.entity.Company;
import ftn.poslovnainformatika.warehouse.entity.Employee;
import ftn.poslovnainformatika.warehouse.entity.Warehouse;
import ftn.poslovnainformatika.warehouse.service.CompanyServiceInterface;
import ftn.poslovnainformatika.warehouse.service.EmployeeServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class WarehouseDTOToWarehouse implements Converter<WarehouseDTO,Warehouse> {

    @Autowired
    EmployeeServiceInterface employeeService;

    @Autowired
    CompanyServiceInterface companyService;

    @Override
    public Warehouse convert(WarehouseDTO warehouseDTO) {
        Warehouse w = new Warehouse();
        w.setId(warehouseDTO.getId());
        w.setName(warehouseDTO.getName());

        Employee employee = employeeService.findOne(warehouseDTO.getEmployee().getId());
        if(employee != null){
            w.setEmployee(employee);
        }

        Company company = companyService.findOne(warehouseDTO.getCompany().getId());
        if(company != null){
            w.setCompany(company);
        }

        return w;
    }
}
