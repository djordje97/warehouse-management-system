package ftn.poslovnainformatika.warehouse.dto.support;

import ftn.poslovnainformatika.warehouse.dto.ProductCategoryDTO;
import ftn.poslovnainformatika.warehouse.entity.Product;
import ftn.poslovnainformatika.warehouse.entity.ProductCategory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProductCategoryToProductCategoryDTOConverter implements Converter<ProductCategory,ProductCategoryDTO> {
    @Override
    public ProductCategoryDTO convert(ProductCategory productCategory) {
        ProductCategoryDTO dto = new ProductCategoryDTO();
        dto.setId(productCategory.getId());
        dto.setProductCategoryName(productCategory.getProductCategoryName());

        return dto;
    }

    public List<ProductCategoryDTO> convert(List<ProductCategory> productCategories){
        List<ProductCategoryDTO> retVal = new ArrayList<>();
        for(ProductCategory pc:productCategories){
            retVal.add(convert(pc));
        }

        return retVal;
    }
}
