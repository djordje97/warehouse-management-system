package ftn.poslovnainformatika.warehouse.dto.support;

import ftn.poslovnainformatika.warehouse.dto.BusinessPartnerDTO;
import ftn.poslovnainformatika.warehouse.dto.BusinessYearDTO;
import ftn.poslovnainformatika.warehouse.dto.DocumentDTO;
import ftn.poslovnainformatika.warehouse.dto.WarehouseDTO;
import ftn.poslovnainformatika.warehouse.entity.Document;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import javax.print.Doc;
import java.util.ArrayList;
import java.util.List;

@Component
public class DocumentToDocumentDTOConverter implements Converter<Document, DocumentDTO> {


    @Override
    public DocumentDTO convert(Document document) {
        DocumentDTO dto = new DocumentDTO();
        dto.setId(document.getId());
        dto.setBookingDate(document.getBookingDate());
        dto.setCreateDate(document.getCreateDate());
        dto.setSerialNumber(document.getSerialNumber());
        dto.setStatus(document.getStatus());
        dto.setType(document.getType());
        dto.setBusinessPartner(new BusinessPartnerDTO(document.getBusinessPartner()));
        dto.setWarehouse(new WarehouseDTO(document.getWarehouse()));
        dto.setBusinessYear(new BusinessYearDTO(document.getBusinessYear()));
        return dto;
    }

    public List<DocumentDTO> convert(List<Document> documents){
        List<DocumentDTO> retVal = new ArrayList<>();
        for (Document d:documents){
            retVal.add(convert(d));
        }

        return retVal;
    }
}
