package ftn.poslovnainformatika.warehouse.dto;

import ftn.poslovnainformatika.warehouse.entity.DocumentItem;

import javax.validation.constraints.NotNull;

public class DocumentItemDTO {

    private Integer id;
    @NotNull(message = "Quantity cannot be empty")
    private double quantity;
    @NotNull(message = "Price cannot be empty")
    private double price;
    @NotNull(message = "Value cannot be empty")
    private double value;
    private DocumentDTO document;
    private  ProductDTO product;

    public DocumentItemDTO() {
        super();
    }

    public DocumentItemDTO(Integer id, double quantity, double price, double value, DocumentDTO document, ProductDTO product) {
        this.id = id;
        this.quantity = quantity;
        this.price = price;
        this.value = value;
        this.document = document;
        this.product = product;
    }

    public DocumentItemDTO(DocumentItem documentItem){
        this(documentItem.getId(),
                documentItem.getQuantity(),
                documentItem.getPrice(),
                documentItem.getValue(),
                new DocumentDTO(documentItem.getDocument()),
                new ProductDTO(documentItem.getProduct()));
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public DocumentDTO getDocument() {
        return document;
    }

    public void setDocument(DocumentDTO document) {
        this.document = document;
    }

    public ProductDTO getProduct() {
        return product;
    }

    public void setProduct(ProductDTO product) {
        this.product = product;
    }
}
