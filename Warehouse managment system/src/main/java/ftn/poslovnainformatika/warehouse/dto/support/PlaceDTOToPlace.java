package ftn.poslovnainformatika.warehouse.dto.support;

import ftn.poslovnainformatika.warehouse.dto.PlaceDTO;
import ftn.poslovnainformatika.warehouse.entity.Place;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class PlaceDTOToPlace implements Converter<PlaceDTO,Place> {
    @Override
    public Place convert(PlaceDTO placeDTO) {
        Place p = new Place();
        p.setId(placeDTO.getId());
        p.setName(placeDTO.getName());
        p.setZipCode(placeDTO.getZipCode());

        return p;
    }
}
