package ftn.poslovnainformatika.warehouse.dto.support;

import ftn.poslovnainformatika.warehouse.dto.DocumentDTO;
import ftn.poslovnainformatika.warehouse.entity.BusinessPartner;
import ftn.poslovnainformatika.warehouse.entity.BusinessYear;
import ftn.poslovnainformatika.warehouse.entity.Document;
import ftn.poslovnainformatika.warehouse.entity.Warehouse;
import ftn.poslovnainformatika.warehouse.service.BusinessPartnerServiceInterface;
import ftn.poslovnainformatika.warehouse.service.BusinessYearServiceInterface;
import ftn.poslovnainformatika.warehouse.service.WarehouseServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class DocumentDTOToDocument implements Converter<DocumentDTO,Document> {

    @Autowired
    private WarehouseServiceInterface warehouseService;

    @Autowired
    private BusinessPartnerServiceInterface businessPartnerService;

    @Autowired
    private BusinessYearServiceInterface businessYearService;

    @Override
    public Document convert(DocumentDTO documentDTO) {
        Document d = new Document();
        d.setId(documentDTO.getId());
        d.setBookingDate(documentDTO.getBookingDate());
        d.setCreateDate(documentDTO.getCreateDate());
        d.setSerialNumber(documentDTO.getSerialNumber());
        d.setStatus(documentDTO.getStatus());
        d.setType(documentDTO.getType());

        Warehouse warehouse = warehouseService.findOne(documentDTO.getWarehouse().getId());
        if(warehouse != null){
            d.setWarehouse(warehouse);
        }

        BusinessPartner businessPartner = businessPartnerService.findOne(documentDTO.getBusinessPartner().getId());
        if(businessPartner != null){
            d.setBusinessPartner(businessPartner);
        }

        BusinessYear businessYear=businessYearService.findOne(documentDTO.getBusinessYear().getId());
        if(businessYear != null){
            d.setBusinessYear(businessYear);
        }
        return d;
    }
}
