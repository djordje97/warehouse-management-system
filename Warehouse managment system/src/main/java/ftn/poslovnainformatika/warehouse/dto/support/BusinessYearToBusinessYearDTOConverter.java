package ftn.poslovnainformatika.warehouse.dto.support;

import ftn.poslovnainformatika.warehouse.dto.BusinessYearDTO;
import ftn.poslovnainformatika.warehouse.entity.BusinessYear;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.sql.BatchUpdateException;
import java.util.ArrayList;
import java.util.List;

@Component
public class BusinessYearToBusinessYearDTOConverter implements Converter<BusinessYear,BusinessYearDTO> {
    @Override
    public BusinessYearDTO convert(BusinessYear businessYear) {
        BusinessYearDTO dto = new BusinessYearDTO();
        dto.setId(businessYear.getId());
        dto.setYear(businessYear.getYear());
        dto.setClosed(businessYear.isClosed());

        return dto;
    }

    public List<BusinessYearDTO> convert(List<BusinessYear> businessYears){
        List<BusinessYearDTO> retVal = new ArrayList<>();
        for(BusinessYear by: businessYears){
            retVal.add(convert(by));
        }

        return retVal;
    }
}
