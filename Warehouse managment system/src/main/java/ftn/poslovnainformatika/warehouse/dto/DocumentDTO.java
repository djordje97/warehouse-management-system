package ftn.poslovnainformatika.warehouse.dto;

import ftn.poslovnainformatika.warehouse.entity.Document;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class DocumentDTO {

    private Integer id;
    @NotNull(message = "Serial number cannot be empty")
    private Integer serialNumber;
    @NotNull(message = "Create date cannot be empty")
    private Date createDate;

    private Date bookingDate;
    @NotBlank(message = "Document status cannot be empty")
    private String status;
    @NotBlank(message = "Document type cannot be empty")
    private String type;
    private BusinessPartnerDTO businessPartner;
    private WarehouseDTO warehouse;
    private  BusinessYearDTO businessYear;

    public DocumentDTO() {
        super();
    }

    public DocumentDTO(Integer id, Integer serialNumber, Date createDate, Date bookingDate, String status, String type, BusinessPartnerDTO businessPartner, WarehouseDTO warehouse, BusinessYearDTO businessYear) {
        this.id = id;
        this.serialNumber = serialNumber;
        this.createDate = createDate;
        this.bookingDate = bookingDate;
        this.status = status;
        this.type = type;
        this.businessPartner = businessPartner;
        this.warehouse = warehouse;
        this.businessYear = businessYear;
    }

    public DocumentDTO(Document document){
        this(document.getId(),
                document.getSerialNumber(),
                document.getCreateDate(),
                document.getBookingDate(),
                document.getStatus(),
                document.getType(),
                new BusinessPartnerDTO(document.getBusinessPartner()),
                new WarehouseDTO(document.getWarehouse()),
                new BusinessYearDTO(document.getBusinessYear()));
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(Integer serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(Date bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BusinessPartnerDTO getBusinessPartner() {
        return businessPartner;
    }

    public void setBusinessPartner(BusinessPartnerDTO businessPartner) {
        this.businessPartner = businessPartner;
    }

    public WarehouseDTO getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(WarehouseDTO warehouse) {
        this.warehouse = warehouse;
    }

    public BusinessYearDTO getBusinessYear() {
        return businessYear;
    }

    public void setBusinessYear(BusinessYearDTO businessYear) {
        this.businessYear = businessYear;
    }
}
