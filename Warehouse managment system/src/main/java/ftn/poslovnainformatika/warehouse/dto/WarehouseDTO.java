package ftn.poslovnainformatika.warehouse.dto;


import ftn.poslovnainformatika.warehouse.entity.Warehouse;

import javax.validation.constraints.NotBlank;

public class WarehouseDTO {

    private Integer id;
    @NotBlank(message = "Name of warehouse cannot be empty")
    private String name;
    private CompanyDTO company;
    private EmployeeDTO employee;

    public WarehouseDTO() {
        super();
    }

    public WarehouseDTO(Integer id, String name, CompanyDTO company, EmployeeDTO employee) {
        super();
        this.id = id;
        this.name = name;
        this.company = company;
        this.employee = employee;
    }

    public WarehouseDTO(Warehouse warehouse){
        this(warehouse.getId(),
                warehouse.getName(),
                new CompanyDTO(warehouse.getCompany()),
                new EmployeeDTO(warehouse.getEmployee()));
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CompanyDTO getCompany() {
        return company;
    }

    public void setCompany(CompanyDTO company) {
        this.company = company;
    }

    public EmployeeDTO getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeDTO employee) {
        this.employee = employee;
    }
}
