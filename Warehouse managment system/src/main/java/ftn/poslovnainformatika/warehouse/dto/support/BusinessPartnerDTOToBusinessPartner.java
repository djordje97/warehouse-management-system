package ftn.poslovnainformatika.warehouse.dto.support;

import ftn.poslovnainformatika.warehouse.dto.BusinessPartnerDTO;
import ftn.poslovnainformatika.warehouse.entity.BusinessPartner;
import ftn.poslovnainformatika.warehouse.entity.Place;
import ftn.poslovnainformatika.warehouse.service.PlaceServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class BusinessPartnerDTOToBusinessPartner implements Converter<BusinessPartnerDTO,BusinessPartner> {

    @Autowired
    private PlaceServiceInterface placeService;

    @Override
    public BusinessPartner convert(BusinessPartnerDTO businessPartnerDTO) {
        BusinessPartner bp = new BusinessPartner();
        bp.setId(businessPartnerDTO.getId());
        bp.setAddress(businessPartnerDTO.getAddress());
        bp.setPib(businessPartnerDTO.getPIB());
        bp.setName(businessPartnerDTO.getName());
        bp.setPartnerType(businessPartnerDTO.getPartnerType());

        Place place = placeService.findByName(businessPartnerDTO.getPlace().getName());
        if(place != null){
            bp.setPlace(place);
        }
        return bp;
    }
}
