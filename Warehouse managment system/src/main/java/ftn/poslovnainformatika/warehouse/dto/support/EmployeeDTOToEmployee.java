package ftn.poslovnainformatika.warehouse.dto.support;

import ftn.poslovnainformatika.warehouse.dto.EmployeeDTO;
import ftn.poslovnainformatika.warehouse.entity.Employee;
import ftn.poslovnainformatika.warehouse.entity.Place;
import ftn.poslovnainformatika.warehouse.service.PlaceServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class EmployeeDTOToEmployee implements Converter<EmployeeDTO,Employee> {

    @Autowired
    PlaceServiceInterface placeService;

    @Override
    public Employee convert(EmployeeDTO employeeDTO) {
        Employee e = new Employee();
        e.setId(employeeDTO.getId());
        e.setAddress(employeeDTO.getAddress());
        e.setFirstName(employeeDTO.getFirstName());
        e.setLastName(employeeDTO.getLastName());
        e.setJMBG(employeeDTO.getJMBG());
        e.setPassword(employeeDTO.getPassword());
        e.setUsername(employeeDTO.getUsername());

        Place place = placeService.findByName(employeeDTO.getPlace().getName());
        if(place != null){
            e.setPlace(place);
        }

        return e;
    }
}
