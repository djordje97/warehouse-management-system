package ftn.poslovnainformatika.warehouse.dto;

import ftn.poslovnainformatika.warehouse.entity.DocumentItem;
import ftn.poslovnainformatika.warehouse.entity.Product;

public class DocumentItemPrintDTO {
    private Integer id;
    private double quantity;
    private double price;
    private double value;
    private DocumentDTO document;
    private Integer articleId;
    private String articleName;

    public DocumentItemPrintDTO() {
    }

    public DocumentItemPrintDTO(Integer id, double quantity, double price, double value, DocumentDTO document, Integer articleId, String articleName) {
        this.id = id;
        this.quantity = quantity;
        this.price = price;
        this.value = value;
        this.document = document;
        this.articleId = articleId;
        this.articleName = articleName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public DocumentDTO getDocument() {
        return document;
    }

    public void setDocument(DocumentDTO document) {
        this.document = document;
    }

    public Integer getArticleId() {
        return articleId;
    }

    public void setArticleId(Integer articleId) {
        this.articleId = articleId;
    }

    public String getArticleName() {
        return articleName;
    }

    public void setArticleName(String articleName) {
        this.articleName = articleName;
    }
}
