INSERT  INTO place(name,zip_code)VALUES ('Beograd',101801);
INSERT  INTO place(name,zip_code)VALUES ('Loznica ',15300);
INSERT INTO business_year(year, is_closed)VALUES (2019, false);

INSERT INTO employee(first_name,last_name,address,jmbg,place_id,username,password)VALUES ('Petar','Petrovic','Novosadskog sajma 6','0507998189987',1,'pera','$2a$12$LtbYtEzOBdnsiD/E9Wtj2OEpK7kd3L7dHa5VoIzukKvjBWlReBAAW')

INSERT  INTO company(name,pib,place_id,employee_id)VALUES ('Merkator','05448855',1,1)

INSERT  INTO warehouse(name,company_id,employee_id) VALUES ('Glavni',1,1);
INSERT  INTO warehouse(name,company_id,employee_id) VALUES ('Pomocni',1,1);
INSERT  INTO warehouse(name,company_id,employee_id) VALUES ('Veoma bitan magacin',1,1);

INSERT  INTO business_partner(name,pib,address,type,place_id) VALUES ('Stark','15875698','Zmaj Jovina 50','Konditorski proizvodi',1);
INSERT  INTO business_partner(name,pib,address,type,place_id) VALUES ('Nelly','158758758','Cvijiceva 5','Konditorski proizvodi',2);
INSERT  INTO measurement_unit(name) VALUES ('g');
INSERT  INTO measurement_unit(name) VALUES ('l');

INSERT INTO  document (serial_number,create_date,booking_date,status,type,business_partner_id,warehouse_id,business_year_id)VALUES ('001','2019-01-22','2019-01-22','Proknjizeno','Primka',1,1,1);
INSERT INTO  document (serial_number,create_date,booking_date,status,type,business_partner_id,warehouse_id,business_year_id)VALUES ('002','2019-01-23','2019-01-23','Proknjizeno','Primka',1,2,1);

INSERT INTO product_category(product_category_name)VALUES ('Glavna kategorija');
INSERT INTO product_card(initial_state_quantity,initial_state_value,traffic_entry_quantity,traffic_entry_value,traffic_exit_quantity,traffic_exit_value,total_quantity,total_value,business_year_id,warehouse_id,purchase_price,price)VALUES (300, 15000, 10, 500, 0, 0, 0, 15000, 1, 1,50,50);
INSERT INTO product_card(initial_state_quantity,initial_state_value,traffic_entry_quantity,traffic_entry_value,traffic_exit_quantity,traffic_exit_value,total_quantity,total_value,business_year_id,warehouse_id,purchase_price,price)VALUES (150.25, 250.60, 170.50, 280.60, 190.30, 290.85, 190.00, 300.02, 1, 1,50,54);
INSERT INTO product_card(initial_state_quantity,initial_state_value,traffic_entry_quantity,traffic_entry_value,traffic_exit_quantity,traffic_exit_value,total_quantity,total_value,business_year_id,warehouse_id,purchase_price,price)VALUES (1500.25, 2500.60, 1700.50, 2800.60, 1900.30, 2900.85, 1900.00, 3000.02, 1, 2,50,55);

INSERT INTO product(product_name,measurement_unit_id,product_card_id,product_category_id)VALUES('Cokolada',1,1,1);
INSERT INTO product(product_name,measurement_unit_id,product_card_id,product_category_id)VALUES('Kafa',1,2,1);
INSERT INTO product(product_name,measurement_unit_id,product_card_id,product_category_id)VALUES('Coca Cola',2,3,1);

INSERT  INTO document_item(quantity,price,value,document_id,product_id)VALUES (30,65.7,18000,1,1);
INSERT  INTO document_item(quantity,price,value,document_id,product_id)VALUES (300,650.7,180000,2,2);
INSERT  INTO document_item(quantity,price,value,document_id,product_id)VALUES (300,650.7,180000,1,3);
INSERT  INTO document_item(quantity,price,value,document_id,product_id)VALUES (150,650.7,97605,1,3);




INSERT  INTO  warehouse_card_analytic(direction,ordinal_number,price,quantity,total_value,traffic_type,product_card_id,document_item_id)VALUES ('U',001,3000,10,30000,'PS',1,1);
INSERT  INTO  warehouse_card_analytic(direction,ordinal_number,price,quantity,total_value,traffic_type,product_card_id,document_item_id)VALUES ('U',002,3000,20,60000,'PR',2,2);
INSERT  INTO  warehouse_card_analytic(direction,ordinal_number,price,quantity,total_value,traffic_type,product_card_id,document_item_id)VALUES ('U',003,3000,30,90000,'PR',3,3);
INSERT  INTO  warehouse_card_analytic(direction,ordinal_number,price,quantity,total_value,traffic_type,product_card_id,document_item_id)VALUES ('U',004,3000,30,90000,'PR',3,4);