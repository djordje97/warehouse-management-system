import { Component, OnInit } from '@angular/core';
import {formatDate } from '@angular/common';
import { WarehouseService } from '../services/warehouse/warehouse.service';
import { BusinessPartnerService } from '../services/businessPartner/business-partner.service';
import { Router } from '@angular/router';
import { BusinessYearService } from '../services/businessYear/business-year.service';

@Component({
  selector: 'app-add-document',
  templateUrl: './add-document.component.html',
  styleUrls: ['./add-document.component.css']
})
export class AddDocumentComponent implements OnInit {

  
  document={
    createDate:formatDate(new Date(),'dd.MM.yyyy', 'en-US'),
    type:'Primka',
    warehouse:new Object({}),
    businessPartner:new Object({}),
    serialNumber:'',
    bookingDate:'',
    status:'Formiranje',
    businessYear:new Object({})
  };
  partnerType;
  warehouses;
  warehouseId;
  partnerId;
  partners;
  businessYear;
  message=false;
  constructor(private warehouseService:WarehouseService,private businessPartnerService:BusinessPartnerService,private router:Router,private businessYearService:BusinessYearService) { }

  ngOnInit() {
    this.warehouseService.getAllWarehouse().subscribe(data =>{
      this.warehouses=data;
      this.document.warehouse=this.warehouses[0];
      this.warehouseId=this.warehouses[0].id;
    });
    this.businessPartnerService.getAllPartners().subscribe(data =>{
      this.partners=data;
      this.document.businessPartner=this.partners[0];
      this.partnerType=this.partners[0].partnerType;
      this.partnerId=this.partners[0].id;
    });

    this.businessYearService.getAll().subscribe(response =>{
      this.businessYear=response[0];
      this.document.businessYear=this.businessYear;
    });
  }
 
  change(event){
    this.warehouseId=event.target.value;
    this.document.warehouse=this.warehouses.filter(x => x.id == this.warehouseId)[0];
  }

  changePartner(event){
    this.partnerId=event.target.value;
    this.document.businessPartner=this.partners.filter(x => x.id == this.partnerId)[0];
    this.partnerType=this.partners.filter(x => x.id == this.partnerId)[0].partnerType;
  }

  goToDocumentItem(){
    if(this.document.serialNumber==""){
      this.message=true;
    }else{
      localStorage.setItem("document",JSON.stringify(this.document));
      this.router.navigate(["/documentItem"]);
    }
  }
}
