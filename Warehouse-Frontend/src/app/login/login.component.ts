import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from '../services/users/users.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user = {
    'username':'',
    'password':''
  };
  message = false;

  constructor(private router:Router, private usersService:UsersService) { }

  ngOnInit() {
  }

  login(){
    if(this.user.username != "" && this.user.password != ""){
      this.usersService.login(this.user).subscribe(response => {
          localStorage.setItem("token",JSON.stringify(response.access_token));
          this.router.navigate(['/home']);
      },error =>{
        this.message=true;
      });
    }else{
      this.message = true;
    }
  }


}
