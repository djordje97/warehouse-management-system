import { Component, OnInit } from '@angular/core';
import { DocumentService } from '../services/document/document.service';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.css']
})
export class DocumentsComponent implements OnInit {

  documents;
  totalPageNumber;
  activePage=0;
  constructor(private documentService:DocumentService) { }

  ngOnInit() {
    this.documentService.getAllDocuments(0).subscribe(data=>{
      this.documents=data.body;
      this.totalPageNumber=Array(Number.parseInt(data.headers.get("totalPages"))).fill(0).map((x,i)=>i);
    });
  }

  nextPage(event){
    let pageNum=event.target.name;
    document.getElementById(this.activePage.toString()).classList.remove("active");
    this.activePage=pageNum;
    this.documentService.getAllDocuments(pageNum).subscribe((data:HttpResponse<any>) => {
    this.documents = data.body;
    console.log(data.headers.keys());
    this.totalPageNumber=Array(Number.parseInt(data.headers.get("totalPages"))).fill(0).map((x,i)=>i); 
    });
    document.getElementById(this.activePage.toString()).classList.add("active");
  }

}
