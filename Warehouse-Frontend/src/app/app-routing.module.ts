import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CodeArticleComponent } from './code-article/code-article.component';
import { LoginComponent } from './login/login.component';
import { ProductCardComponent } from './product-card/product-card.component';
import { DocumentComponent } from './document/document.component';
import { DocumentItemComponent } from './document-item/document-item.component';
import { DocumentsComponent } from './documents/documents.component';
import { AddDocumentComponent } from './add-document/add-document.component';
import { AddProductComponent } from './add-product/add-product.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'codeArticle',
    component: CodeArticleComponent
  },
  {
    path: 'productCard/:id',
    component: ProductCardComponent
  },
  {
    path: 'document/:id',
    component: DocumentComponent
  },
  {
    path: 'documentItem',
    component: DocumentItemComponent
  },
  {
    path: 'documents',
    component: DocumentsComponent
  },
  {
    path: 'addDocument',
    component: AddDocumentComponent
  },
  {
    path: 'addProduct',
    component: AddProductComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
