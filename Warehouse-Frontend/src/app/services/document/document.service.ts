import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {

  url="http://localhost:8080/api/documents";
  constructor(private http:HttpClient) { }

  getAllDocuments(pageNum){
    var head;
    var token=JSON.parse(localStorage.getItem("token"));
    if(token){
      head={
          "Authorization": "Bearer " +token,
          'Content-Type': 'application/json'
        };
      }else{
          head={
              'Content-Type': 'application/json'
          };
      }
     let  httpOptions= {
          header: new  HttpHeaders(head)
      };

     return this.http.get(this.url+"?pageNum="+pageNum, {headers:httpOptions.header,observe:'response'});
  }

  getDocumentById(id:number){
    var head;
    var token=JSON.parse(localStorage.getItem("token"));
    if(token){
      head={
          "Authorization": "Bearer " +token,
          'Content-Type': 'application/json'
        };
      }else{
          head={
              'Content-Type': 'application/json'
          };
      }
     let  httpOptions= {
          header: new  HttpHeaders(head)
      };

     return this.http.get(this.url+"/"+id, {headers:httpOptions.header});
  } 

  addDcument(document:any){
    var head;
   
    var token=JSON.parse(localStorage.getItem("token"));
    if(token){
      head={
          "Authorization": "Bearer " +token,
          'Content-Type': 'application/json'
        };
      }else{
          head={
              'Content-Type': 'application/json'
          };
      }
     let  httpOptions= {
          header: new  HttpHeaders(head)
      };

     return this.http.post(this.url,document, {headers:httpOptions.header});
  }

  addDocumentItem(documentItem,productId){
    var head;
    let urlDI="http://localhost:8080/api/documentItems";
    var token=JSON.parse(localStorage.getItem("token"));
    if(token){
      head={
          "Authorization": "Bearer " +token,
          'Content-Type': 'application/json'
        };
      }else{
          head={
              'Content-Type': 'application/json'
          };
      }
     let  httpOptions= {
          header: new  HttpHeaders(head)
      };

     return this.http.post(urlDI,documentItem, {headers:httpOptions.header});
  }

  getItemByDocument(documentId){
    var head;
    let urlDI="http://localhost:8080/api/documentItems"
    var token=JSON.parse(localStorage.getItem("token"));
    if(token){
      head={
          "Authorization": "Bearer " +token,
          'Content-Type': 'application/json'
        };
      }else{
          head={
              'Content-Type': 'application/json'
          };
      }
     let  httpOptions= {
          header: new  HttpHeaders(head)
      };

     return this.http.get(urlDI+"/document/"+documentId, {headers:httpOptions.header});
  }

  editDocument(document){
    var head;
   
    var token=JSON.parse(localStorage.getItem("token"));
    if(token){
      head={
          "Authorization": "Bearer " +token,
          'Content-Type': 'application/json'
        };
      }else{
          head={
              'Content-Type': 'application/json'
          };
      }
     let  httpOptions= {
          header: new  HttpHeaders(head)
      };

     return this.http.put(this.url+'/'+document.id,document, {headers:httpOptions.header});
  }
}
