import { TestBed } from '@angular/core/testing';

import { AnaliticCardService } from './analitic-card.service';

describe('AnaliticCardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AnaliticCardService = TestBed.get(AnaliticCardService);
    expect(service).toBeTruthy();
  });
});
