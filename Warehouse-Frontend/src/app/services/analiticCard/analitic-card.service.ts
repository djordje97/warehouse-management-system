import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AnaliticCardService {

  constructor(private http:HttpClient) { }

  url="http://localhost:8080/api/warehouseCardAnalytics";

  getAnaliticCardsByProductCardId(id:number){
    var head;
    var token=JSON.parse(localStorage.getItem("token"));
    if(token){
      head={
          "Authorization": "Bearer " +token,
          'Content-Type': 'application/json'
        };
      }else{
          head={
              'Content-Type': 'application/json'
          };
      }
     let  httpOptions= {
          header: new  HttpHeaders(head)
      };

     return this.http.get(this.url+"/productCard/"+id, {headers:httpOptions.header});
  } 


}
