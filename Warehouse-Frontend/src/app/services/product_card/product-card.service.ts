import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductCardService {

  constructor(private http: HttpClient) { }

  url = "http://localhost:8080/api/productCard";

  addProductCard(productCard:any){
    var head;
    var token=JSON.parse(localStorage.getItem("token"));
    if(token){
      head={
          "Authorization": "Bearer " +token,
          'Content-Type': 'application/json'
        };
      }else{
          head={
              'Content-Type': 'application/json'
          };
      }
     let  httpOptions= {
          header: new  HttpHeaders(head)
      };

     return this.http.post(this.url,productCard, {headers:httpOptions.header});
  }

  editProductCard(id:number){
    var head;
    var token=JSON.parse(localStorage.getItem("token"));
    if(token){
      head={
          "Authorization": "Bearer " +token,
          'Content-Type': 'application/json'
        };
      }else{
          head={
              'Content-Type': 'application/json'
          };
      }
     let  httpOptions= {
          header: new  HttpHeaders(head)
      };

     return this.http.put(this.url+"/"+id, {headers:httpOptions.header});
  }

  print(productId,startDate,endDate){
    var head;
    let printUrl="http://localhost:8080/api/reports/analytic?productId="+productId+"&startDate="+startDate+"&endDate="+endDate;
    var token=JSON.parse(localStorage.getItem("token"));
    if(token){
      head={
          "Authorization": "Bearer " +token
        };
      }else{
          head={
              'Content-Type': 'application/json'
          };
      }
     let  httpOptions= {
          header: new  HttpHeaders(head)
      };

     return this.http.get(printUrl, {headers:httpOptions.header,responseType:'blob',observe:'response'});
  }
}
