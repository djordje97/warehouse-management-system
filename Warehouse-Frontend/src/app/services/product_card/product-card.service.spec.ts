import { TestBed } from '@angular/core/testing';

import { ProductCardService } from './product-card.service';

describe('ProductCardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductCardService = TestBed.get(ProductCardService);
    expect(service).toBeTruthy();
  });
});
