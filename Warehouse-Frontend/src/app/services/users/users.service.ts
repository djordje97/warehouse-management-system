import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http:HttpClient) { }

  url="http://localhost:8080/api/"

  login(user):any{
    return this.http.post(this.url+"auth/login", user);
  }

}
