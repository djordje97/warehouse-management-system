import { TestBed } from '@angular/core/testing';

import { CodeArticleService } from './code-article.service';

describe('CodeArticleService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CodeArticleService = TestBed.get(CodeArticleService);
    expect(service).toBeTruthy();
  });
});
