import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CodeArticleService {

  constructor(private http:HttpClient) { }

  url="http://localhost:8080/api/products";

  getAllCodeArticles(pageNum){
    var head;
    var token=JSON.parse(localStorage.getItem("token"));
    if(token){
      head={
          "Authorization": "Bearer " +token,
          'Content-Type': 'application/json',
          'Observe':'response'
        };
      }else{
          head={
              'Content-Type': 'application/json',
          };
      }
     let  httpOptions= {
          header: new  HttpHeaders(head),
      };

     return this.http.get(this.url+"?pageNum="+pageNum, {headers:httpOptions.header,observe:'response'});
  }

  getProductById(id:number){
    var head;
    var token=JSON.parse(localStorage.getItem("token"));
    if(token){
      head={
          "Authorization": "Bearer " +token,
          'Content-Type': 'application/json'
        };
      }else{
          head={
              'Content-Type': 'application/json'
          };
      }
     let  httpOptions= {
          header: new  HttpHeaders(head)
      };

     return this.http.get(this.url+"/"+id, {headers:httpOptions.header});
  } 
  
  getWarehouseProductById(id:number,pageNum){
    var head;
    var token=JSON.parse(localStorage.getItem("token"));
    if(token){
      head={
          "Authorization": "Bearer " +token,
          'Content-Type': 'application/json'
        };
      }else{
          head={
              'Content-Type': 'application/json'
          };
      }
     let  httpOptions= {
          header: new  HttpHeaders(head)
      };

     return this.http.get(this.url+"/warehouse/"+id+"/"+pageNum, {headers:httpOptions.header,observe:'response'});
  } 

  search(text,pageNum){
    var head;
    var token=JSON.parse(localStorage.getItem("token"));
    if(token){
      head={
          "Authorization": "Bearer " +token,
          'Content-Type': 'application/json'
        };
      }else{
          head={
              'Content-Type': 'application/json'
          };
      }
     let  httpOptions= {
          header: new  HttpHeaders(head)
      };

     return this.http.get(this.url+"/search?text="+text+"&pageNum="+pageNum, {headers:httpOptions.header,observe:'response'});
  }

  print(warehouseName){
    var head;
    let printUrl="http://localhost:8080/api/reports/lagerLista?name="+warehouseName;
    var token=JSON.parse(localStorage.getItem("token"));
    if(token){
      head={
          "Authorization": "Bearer " +token
        };
      }else{
          head={
              'Content-Type': 'application/json'
          };
      }
     let  httpOptions= {
          header: new  HttpHeaders(head)
      };

     return this.http.get(printUrl, {headers:httpOptions.header,responseType:'blob',observe:'response'});
  }
}
