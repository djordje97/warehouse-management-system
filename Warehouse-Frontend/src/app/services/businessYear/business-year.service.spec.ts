import { TestBed } from '@angular/core/testing';

import { BusinessYearService } from './business-year.service';

describe('BusinessYearService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BusinessYearService = TestBed.get(BusinessYearService);
    expect(service).toBeTruthy();
  });
});
