import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WarehouseService {

  constructor(private http: HttpClient) { }

  url="http://localhost:8080/api/warehouse";

  getAllWarehouse(){
    var head;
    var token=JSON.parse(localStorage.getItem("token"));
    if(token){
      head={
          "Authorization": "Bearer " +token,
          'Content-Type': 'application/json'
        };
      }else{
          head={
              'Content-Type': 'application/json'
          };
      }
     let  httpOptions= {
          header: new  HttpHeaders(head)
      };

     return this.http.get(this.url, {headers:httpOptions.header});
  } 

  getWarehouseById(id:number){
    var head;
    var token=JSON.parse(localStorage.getItem("token"));
    if(token){
      head={
          "Authorization": "Bearer " +token,
          'Content-Type': 'application/json'
        };
      }else{
          head={
              'Content-Type': 'application/json'
          };
      }
     let  httpOptions= {
          header: new  HttpHeaders(head)
      };

     return this.http.get(this.url+"/"+id, {headers:httpOptions.header});
  } 


}
