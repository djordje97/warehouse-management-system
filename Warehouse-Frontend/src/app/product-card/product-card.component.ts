import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { CodeArticleService } from '../services/code_articles/code-article.service';
import { AnaliticCardService } from '../services/analiticCard/analitic-card.service';
import {formatDate } from '@angular/common';
import { ProductCardService } from '../services/product_card/product-card.service';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.css']
})
export class ProductCardComponent implements OnInit {

  constructor(private router:Router, private productService:CodeArticleService, private analiticCardService:AnaliticCardService, private route: ActivatedRoute,
    private productCardService: ProductCardService) { }

  cardID: number;
  product=new Object({});
  overlay = false;
  startDate;
  endDate;

  productCardById;
  analiticCards;
  divDateForPrint = false;

  today= new Date();
  jstoday = '';

  message = false;

  ngOnInit() {
    
    var token=JSON.parse(localStorage.getItem("token"));
    if(!token){
      this.router.navigate(["/"]);
    }

    this.route.params.subscribe(params =>{
      this.cardID=params["id"];
      console.log(this.cardID); 
    });

    this.productService.getProductById(this.cardID).subscribe(data => {
      this.product = data;
      console.log(data);
    });

    this.jstoday = formatDate(this.today, 'dd.MM.yyyy', 'en-US');

  }

  showAnaliticCards(){
    document.getElementById("overlay").style.display = "block";

    this.analiticCardService.getAnaliticCardsByProductCardId(this.cardID).subscribe(data => {
      this.analiticCards = data;
    });

  }

  closeAnaliticCards(){
    document.getElementById("overlay").style.display = "none";
    this.divDateForPrint = false;
  }

  openDateForPrint(){
    this.divDateForPrint = true;
  }
  getFormattedDate(date) {
    let year = date.getFullYear();
  
    let month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;
  
    let day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
    
    return day+month+year;
  }

  print(){
    let startDate=new Date(this.startDate);
    let endDate=new Date(this.endDate);
    if(startDate == null || endDate == null){
      return;
    }
    let startDateForSend=this.getFormattedDate(startDate);
    let endDateForSend=this.getFormattedDate(endDate);
    let productId=this.cardID;
    this.productCardService.print(productId,startDateForSend,endDateForSend).subscribe(response =>{
      let filename=response.headers.get("content-disposition").split(' ')[1].split("=")[1];
      var url = window.URL.createObjectURL(response.body);
      var a = document.createElement('a');
      document.body.appendChild(a);
      a.setAttribute('style', 'display: none');
      a.href = url;
      a.download = filename;
      a.click();
      window.URL.revokeObjectURL(url);
      a.remove(); 
    });
    console.log(startDateForSend);
  }

 

}
