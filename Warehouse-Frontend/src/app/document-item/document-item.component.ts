import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from '../services/product/product.service';
import { DocumentService } from '../services/document/document.service';
import { CodeArticleService } from '../services/code_articles/code-article.service';

@Component({
  selector: 'app-document-item',
  templateUrl: './document-item.component.html',
  styleUrls: ['./document-item.component.css']
})
export class DocumentItemComponent implements OnInit {

  constructor(private router:Router,private productService:CodeArticleService,private documentService:DocumentService) {}

  addItem = false;
  document:any;
  products;
  documentItem={
    document:this.document,
    quantity:0,
    value:0,
    price:0,
    product:new Object({})
  };
  documentItems=[];
  selectedProduct:any;

  messageItem = false;
  messageDocument = false;

  ngOnInit() {
    this.document=JSON.parse(localStorage.getItem("document"));
    this.productService.getWarehouseProductById(this.document.warehouse.id,0).subscribe(data =>{
      this.products=data.body;
      this.documentItem.product=this.products[0];
      if(this.document.type == "Primka"){
        this.documentItem.price=this.products[0].productCard.purchasePrice;
      }else{
        console.log(this.products);
        this.documentItem.price=this.products[0].productCard.price;
      }
      console.log(this.products);
    });
  }

  onChange(event){
    this.selectedProduct=this.products.filter(x => x.id == event.target.value)[0];
    this.documentItem.product=this.selectedProduct;
    console.log(this.documentItem.product);
    if(this.document.type == "Primka"){
      this.documentItem.price=this.selectedProduct.productCard.purchasePrice;
    }else{
      this.documentItem.price=this.selectedProduct.productCard.price;
    }
  }
  showAddItem(){
    document.getElementById("addItem").style.display = "block";
  }

  closeAddItem(){
    document.getElementById("addItem").style.display = "none";
  }

  addDocumentItem(){
    if(this.documentItem.quantity <= 0){
      this.messageItem = true;
    }else{
      console.log(this.documentItem);
      var documentForAdd=Object.assign({},this.documentItem);
      this.documentItems.push(documentForAdd);
      this.documentItem.value=0;
      this.documentItem.price=0;
      this.documentItem.quantity=0;
      this.documentItem.product=new Object({});
      this.selectedProduct=new Object({});
      document.getElementById("addItem").style.display = "none";
    }   
  }

  save(){
    if(this.documentItems.length > 0){
      this.document.createDate=new Date();
      console.log(this.document);
      this.documentService.addDcument(this.document).subscribe(data =>{
        this.document=data;
        this.documentItems.forEach(item => {
            let object={
                quantity:item.quantity,
                value:item.value,
                price:item.price,
                document:this.document,
                product:item.product
            };
            this.documentService.addDocumentItem(object,item.articleCode).subscribe(result =>{

            });
        });
        localStorage.removeItem("document");
        this.router.navigate(["/home"]);
      });
      
    }else{
      this.messageDocument = true;
    }
  }
}
