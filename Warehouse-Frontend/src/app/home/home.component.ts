import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CodeArticleService } from '../services/code_articles/code-article.service';
import { WarehouseService } from '../services/warehouse/warehouse.service';
import {formatDate } from '@angular/common';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private router:Router, private codearticleService:CodeArticleService, private warehouseService: WarehouseService) { }

  products;
  totalValue=0;
  warehouses;
  visibility = false;
  warehouseID: number;
  oneWarehouse;
  totalPageNumber;
  activePage=0;

  today= new Date();
  jstoday = '';
  

  ngOnInit() {

    var token=JSON.parse(localStorage.getItem("token"));
    if(!token){
      this.router.navigate(["/"]);
    }

    this.warehouseService.getAllWarehouse().subscribe(data => {
      console.log(data);
      this.warehouses = data;
    });

  }

  showMainDiv(event){
      this.visibility = true;
      this.warehouseID = event.target.id;
      this.totalValue = 0;

      this.jstoday = formatDate(this.today, 'dd.MM.yyyy', 'en-US');

      this.warehouseService.getWarehouseById(this.warehouseID).subscribe(result => {
        this.oneWarehouse = result;
      });

      this.codearticleService.getWarehouseProductById(this.warehouseID,0).subscribe(data => {
        this.products = data.body;
        this.totalPageNumber=Array(Number.parseInt(data.headers.get("totalPages"))).fill(0).map((x,i)=>i);
        console.log(data);
        document.getElementById(this.activePage.toString()).classList.add("active");
        if(this.products){
          this.products.forEach(element => {
            this.totalValue+=element.productCard.totalValue;
          });
         }else{
           this.totalValue=0;
         }
      });
  }

  nextPage(event){
    let pageNum=event.target.name;
    document.getElementById(this.activePage.toString()).classList.remove("active");
    this.activePage=pageNum;
    this. codearticleService.getWarehouseProductById(this.warehouseID,pageNum).subscribe((data:HttpResponse<any>) => {
    this.products = data.body;
    this.totalPageNumber=Array(Number.parseInt(data.headers.get("totalPages"))).fill(0).map((x,i)=>i); 
    });
    document.getElementById(this.activePage.toString()).classList.add("active");
  }

  printLagerList(){
    let name=this.oneWarehouse.name;
    this.codearticleService.print(name).subscribe(response =>{
      let filename=response.headers.get("content-disposition").split(' ')[1].split("=")[1];
      var url = window.URL.createObjectURL(response.body);
      var a = document.createElement('a');
      document.body.appendChild(a);
      a.setAttribute('style', 'display: none');
      a.href = url;
      a.download = filename;
      a.click();
      window.URL.revokeObjectURL(url);
      a.remove(); 
    });
  }
}
