import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CodeArticleComponent } from './code-article.component';

describe('CodeArticleComponent', () => {
  let component: CodeArticleComponent;
  let fixture: ComponentFixture<CodeArticleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CodeArticleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CodeArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
