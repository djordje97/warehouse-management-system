import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CodeArticleService } from '../services/code_articles/code-article.service';
import {formatDate } from '@angular/common';
import { HttpResponse } from '@angular/common/http';
import { identifierModuleUrl } from '@angular/compiler';

@Component({
  selector: 'app-code-article',
  templateUrl: './code-article.component.html',
  styleUrls: ['./code-article.component.css']
})
export class CodeArticleComponent implements OnInit {

  codeArticles;

  today= new Date();
  jstoday = '';
  totalPageNumber;
  activePage=0;
  searchText='';

  refreshButton = false;

  constructor(private router:Router, private codearticleService:CodeArticleService) { }

  ngOnInit() {

    var token=JSON.parse(localStorage.getItem("token"));
    if(!token){
      this.router.navigate(["/"]);
    }

    this.codearticleService.getAllCodeArticles(0).subscribe((data:HttpResponse<any>) => {
      this.codeArticles = data.body;
      this.totalPageNumber=Array(Number.parseInt(data.headers.get("totalPages"))).fill(0).map((x,i)=>i); 
     
    });

    this.jstoday = formatDate(this.today, 'dd.MM.yyyy', 'en-US');
    document.getElementById("0").classList.add("active");
  }
  

  nextPage(event){
    let pageNum=event.target.name;
   if(this.searchText !== ''){
    this.search();
   }
   else{
    document.getElementById(this.activePage.toString()).classList.remove("active");
    this.activePage=pageNum;
    this.codearticleService.getAllCodeArticles(pageNum).subscribe((data:HttpResponse<any>) => {
      this.codeArticles = data.body;
      this.totalPageNumber=Array(Number.parseInt(data.headers.get("totalPages"))).fill(0).map((x,i)=>i); 
    });
    document.getElementById(this.activePage.toString()).classList.add("active");
   }
  }
  goToAddProductPage(){
    this.router.navigate(["/addProduct"]);
  }

  search(){

  this.codearticleService.search(this.searchText,0).subscribe(response =>{
    this.codeArticles=response.body;
    this.totalPageNumber=Array(Number.parseInt(response.headers.get("totalPages"))).fill(0).map((x,i)=>i); 
  })
  this.refreshButton = true;
  }

  refresh(){
    this.codearticleService.getAllCodeArticles(0).subscribe((data:HttpResponse<any>) => {
      this.codeArticles = data.body;
      this.totalPageNumber=Array(Number.parseInt(data.headers.get("totalPages"))).fill(0).map((x,i)=>i); 
     
    });
    this.refreshButton = false;
  }

}
