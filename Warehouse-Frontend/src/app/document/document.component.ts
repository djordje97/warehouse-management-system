import { Component, OnInit } from '@angular/core';
import { DocumentService } from '../services/document/document.service';
import { ActivatedRoute, Router } from '@angular/router';
import { WarehouseService } from '../services/warehouse/warehouse.service';
import { BusinessYearService } from '../services/businessYear/business-year.service';

@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.css']
})
export class DocumentComponent implements OnInit {

  document;
  documentID;
  warehouse;
  documentItems:any;
  constructor(private documentService:DocumentService,private warehouseService: WarehouseService,private route: ActivatedRoute,private router:Router) { }

  documentItem = false;

  ngOnInit() {
    this.route.params.subscribe(params =>{
      this.documentID=params["id"];
      console.log(this.documentID); 
    });
  
    this.documentService.getDocumentById(this.documentID).subscribe(data =>{
      console.log(data);
      this.document=data;
      this.documentService.getItemByDocument(this.documentID).subscribe(result=>{
        this.documentItems=result;
      });
    });
  }

  showDocumentItem(){
    document.getElementById("documentItem").style.display = "block";
  }

  closeDocumentItem(){
    document.getElementById("documentItem").style.display = "none";
  }

  booking(){
    this.document.status='Proknjizeno';
    this.documentService.editDocument(this.document).subscribe(data =>{
    this.router.navigate(["/documents"]);
    });
  }

  cancel(){
    this.document.status="Storno"
    this.documentService.addDcument(this.document).subscribe(data =>{
      this.router.navigate(["/documents"]);
    });
  }
}
