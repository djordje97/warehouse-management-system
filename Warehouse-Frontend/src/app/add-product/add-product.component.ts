import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { WarehouseService } from '../services/warehouse/warehouse.service';
import { ProductService } from '../services/product/product.service';
import { BusinessPartnerService } from '../services/businessPartner/business-partner.service';
import { ProductCardService } from '../services/product_card/product-card.service';
import { MeasurementUnitService } from '../services/measurementUnit/measurement-unit.service';
import { BusinessYearService } from '../services/businessYear/business-year.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  constructor(private router: Router, private warehouseService: WarehouseService, private productService: ProductService,
    private bPartnerService: BusinessPartnerService, private productCardService: ProductCardService,private measurementUnitService:MeasurementUnitService,private businessYearService:BusinessYearService) { }

    productCard = {
      initialStateQuantity: 0,
      initialStateValue: 0,
      trafficEntryQuantity: 0,
      trafficEntryValue: 0,
      trafficExitQuantity: 0,
      trafficExitValue: 0,
      totalQuantity: 0,
      totalValue: 0,
      businessYear: new Object({}),
      warehouse: new Object({}),
      price: 0,
      purchasePrice: 0,
    }

  product = {
    productName: "",
    measurementUnit: new Object({}),
    productCard: this.productCard,
    productCategory: new Object({}),
    businessPartner: new Object({})
  }

  warehouses;
  warehouseID;
  productCategories;
  productCategoryID;
  measurementUnits;
  measurementUnitID;
  businessPartners;
  businessPartnerID;

  message = false;

  ngOnInit() {

    var token=JSON.parse(localStorage.getItem("token"));
    if(!token){
      this.router.navigate(["/"]);
    }

    this.warehouseService.getAllWarehouse().subscribe(data => {
      this.warehouses = data;
      this.productCard.warehouse = this.warehouses[0];
      this.warehouseID = this.warehouses[0].id;
    });

    this.productService.getAllCategory().subscribe(data => {
      this.productCategories = data;
      this.product.productCategory = this.productCategories[0];
      this.productCategoryID = this.productCategories[0].id;
    });

    this.bPartnerService.getAllPartners().subscribe(data => {
      this.businessPartners = data;
      this.product.businessPartner = this.businessPartners[0];
      this.businessPartnerID = this.businessPartners[0].id;
    })


    this.measurementUnitService.getAll().subscribe(data =>{
      this.measurementUnits=data;
      this.product.measurementUnit=this.measurementUnits[0];
      this.measurementUnitID=this.measurementUnits[0].id;
    });

    this.businessYearService.getAll().subscribe(data =>{
      this.productCard.businessYear=data[0];
    });
  }

  changeWarehouse(event){
    this.warehouseID = event.target.value;
    this.productCard.warehouse = this.warehouses.filter(x => x.id == this.warehouseID)[0];
  }

  changeProductCategory(event){
    this.productCategoryID = event.target.value;
    this.product.productCategory = this.productCategories.filter(x => x.id == this.productCategoryID)[0];
  }

  changeBusinessPartner(event){
    this.businessPartnerID = event.target.value;
    this.product.businessPartner = this.businessPartners.filter(x => x.id == this.businessPartnerID)[0];
  }

  changeMeasurementUnit(event){
    this.measurementUnitID=event.target.value;
    this.product.measurementUnit=this.measurementUnits.filter(x => x.id == this.measurementUnitID)[0];
  }

  submitNewPP(){
    if(this.productCard.initialStateQuantity <= 0 || this.productCard.purchasePrice <= 0 || this.product.productName == "" || this.productCard.price <= 0){
      this.message = true;
    }else{
      this.productService.addProduct(this.product, this.product.businessPartner).subscribe(result => {
        this.router.navigate(["/home"]);
     }); 
    }

  }


}
